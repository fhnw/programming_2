package week03.extraBindingExample;

import javafx.application.Platform;
import javafx.beans.property.SimpleBooleanProperty;

public class Extra_Controller {
	final private Extra_Model model;
	final private Extra_View view;
	
	private final SimpleBooleanProperty userInputValid = new SimpleBooleanProperty();
	private final SimpleBooleanProperty modelValueChanged = new SimpleBooleanProperty();

	protected Extra_Controller(Extra_Model model, Extra_View view) {
		this.model = model;
		this.view = view;

		view.txtUserInput.textProperty().addListener(
				// Parameters of any PropertyChangeListener
				(observable, oldValue, newValue) -> validateUserInput(newValue));
		
		// Bind the button for UserInput to our SimpleBooleanProperty
		view.btnUserInput.disableProperty().bind(userInputValid.not());
		
		// Bind the button for ModelValue to check the current value
		view.btnModelValue.disableProperty().bind(modelValueChanged.not());
		
		// register ourselves to listen for property changes in the model.
		model.modelValue.addListener( (observable, oldValue, newValue) -> {
		        String newText = newValue.toString();
		        
		        modelValueChanged.set(!newValue.equals(model.shouldBeValue));
		        
		        // Move to the JavaFX thread, update the View
		        Platform.runLater(new Runnable() {
		            @Override public void run() {
				        view.lblModelValue.setText(newText);
		            }
		        });
			}
        );
		
		// Button action for btnModelValue
		view.btnModelValue.setOnAction(e -> model.resetModelValue());
		
		// Button action for btnUserInput
		view.btnUserInput.setOnAction(e -> view.txtUserInput.setText(""));
	}

	/**
	 * User input at least 5 characters
	 */
	private void validateUserInput(String newValue) {
		userInputValid.set(newValue.length() >= 5);

		view.txtUserInput.getStyleClass().remove("userInputNotOk");
		view.txtUserInput.getStyleClass().remove("userInputOk");
		if (userInputValid.get()) {
			view.txtUserInput.getStyleClass().add("userInputOk");
		} else {
			view.txtUserInput.getStyleClass().add("userInputNotOk");
		}
	}
}
