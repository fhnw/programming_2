package week03.extraBindingExample;

import javafx.application.Application;
import javafx.stage.Stage;

public class Extra_MVC extends Application {
    private Extra_View view;
    private Extra_Controller controller;
    private Extra_Model model;

    @Override
    public void start(Stage primaryStage) throws Exception {
        model = new Extra_Model();
        view = new Extra_View(primaryStage, model);
        controller = new Extra_Controller(model, view);

        // Display the GUI after all initialization is complete
        view.start();
    }

    public static void main(String[] args) {
        launch(args);
    }

}
