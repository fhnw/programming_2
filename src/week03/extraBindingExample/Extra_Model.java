package week03.extraBindingExample;

import javafx.beans.property.SimpleIntegerProperty;

public class Extra_Model {
	final SimpleIntegerProperty modelValue = new SimpleIntegerProperty();
	final int shouldBeValue = 13;
	
	// We start a thread that will randomly change the modelValue;
	public Extra_Model() {
		resetModelValue(); // Set the initial value
		new Thread( () -> {
			while(true) {
				modelValue.set((int) (Math.random() * 100));
				try {
					Thread.sleep(3000);
				} catch (Exception e) {
					
				}
			}
		}).start();
	}
	
	public void resetModelValue() {
		modelValue.set(shouldBeValue);
	}
	
}
