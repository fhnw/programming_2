package week03.extraBindingExample;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class Extra_View {
    private Extra_Model model;
    private Stage stage;

    protected TextField txtUserInput = new TextField();
    protected Label lblModelValue = new Label();
    protected Button btnUserInput = new Button("Save user input");
    protected Button btnModelValue = new Button("Reset model value");

    protected Extra_View(Stage stage, Extra_Model model) {
        this.stage = stage;
        this.model = model;
        
        stage.setTitle("Extra Property Example");
        
        GridPane root = new GridPane();
        txtUserInput = new TextField();
        root.add(txtUserInput, 0, 0); root.add(btnUserInput, 0, 1);
        root.add(lblModelValue, 1, 0); root.add(btnModelValue, 1, 1);

        Scene scene = new Scene(root);
		scene.getStylesheets().add(
				getClass().getResource("Extra.css").toExternalForm());        
        stage.setScene(scene);
    }
    
    public void start() {
        stage.show();
    }
    
    /**
     * Stopping the view - just make it invisible
     */
    public void stop() {
        stage.hide();
    }
    
    /**
     * Getter for the stage, so that the controller can access window events
     */
    public Stage getStage() {
        return stage;
    }
}
