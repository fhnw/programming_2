package week09_DataStructures.solutions.DataStructureTesting;

import java.text.DecimalFormat;

import javafx.event.ActionEvent;
import week09_DataStructures.solutions.DataStructureTesting.DataStructureTestingModel.ACTION_CHOICES;
import week09_DataStructures.solutions.DataStructureTesting.DataStructureTestingModel.COLLECTION_TYPES;

public class DataStructureTestingController {
	private DataStructureTestingModel model;
	private DataStructureTestingView view;

	private final DecimalFormat timeFormatter = new DecimalFormat("0.000");
	
	public DataStructureTestingController(DataStructureTestingModel model, DataStructureTestingView view) {
		this.model = model;
		this.view = view;
		
		view.btnGo.setOnAction(this::setUpTest);
	}
	
	private void setUpTest(ActionEvent e) {
		// Get selections from View
		Integer amountOfData = view.cmbNumElements.getValue();
		COLLECTION_TYPES collectionType = view.cmbCollectionType.getValue();
		ACTION_CHOICES whereChoice = view.cmbOperation.getValue();

		float runTime = model.runTest(amountOfData, collectionType, whereChoice);
		
		view.lblResult.setText("Time: " + timeFormatter.format(runTime) + " seconds");
		
		// Trigger garbage collection
		System.gc();
	}

}
