package week09_DataStructures.solutions.DataStructureTesting;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import week09_DataStructures.solutions.DataStructureTesting.DataStructureTestingModel.ACTION_CHOICES;
import week09_DataStructures.solutions.DataStructureTesting.DataStructureTestingModel.COLLECTION_TYPES;

public class DataStructureTestingView {
	private Stage stage;
	private DataStructureTestingModel model;

	protected final Integer[] DATA_AMOUNTS = {1000,3000,10000,30000,100000,300000,1000000};

	private Label lblNumElements = new Label("Amount of data");
	protected ComboBox<Integer> cmbNumElements = new ComboBox<>();
	private Label lblCollectionType = new Label("Collection type");
	protected ComboBox<COLLECTION_TYPES> cmbCollectionType = new ComboBox<>();
	private Label lblOperation = new Label("Operation");
	protected ComboBox<ACTION_CHOICES> cmbOperation = new ComboBox<>();
	protected Button btnGo = new Button("Run test");
	protected Label lblResult = new Label();
	
	
	public DataStructureTestingView(Stage stage, DataStructureTestingModel model) {
		this.stage = stage;
		this.model = model;

		cmbNumElements.getItems().setAll(DATA_AMOUNTS);
		cmbNumElements.setValue(DATA_AMOUNTS[0]);
		
		cmbCollectionType.getItems().setAll(COLLECTION_TYPES.values());
		cmbCollectionType.setValue(COLLECTION_TYPES.ArrayList);
		
		cmbOperation.getItems().setAll(ACTION_CHOICES.values());
		cmbOperation.setValue(ACTION_CHOICES.Add);
		
		GridPane grid = new GridPane();
		grid.addRow(0, lblNumElements, cmbNumElements);
		grid.addRow(1, lblCollectionType, cmbCollectionType);
		grid.addRow(2, lblOperation, cmbOperation);
		
		VBox root = new VBox(grid, btnGo, lblResult);
		root.getStyleClass().add("vbox");

        Scene scene = new Scene(root);
		scene.getStylesheets().add(
				getClass().getResource("styles.css").toExternalForm());        
        stage.setScene(scene);
        stage.setTitle("Data structure testing");
	}

	public void start() {
		stage.show();
	}

}
