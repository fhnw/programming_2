package week09_DataStructures.solutions.DataStructureTesting;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.TreeSet;

import week09_DataStructures.ListTesting.SampleData;

public class DataStructureTestingModel {
	protected enum COLLECTION_TYPES {ArrayList, LinkedList, TreeSet, HashSet};
	protected enum ACTION_CHOICES {Add, Retrieve};
	
	/**
	 * Run the defined test, returning the total elapsed time in seconds
	 * @param amountOfData The amount of elements to add to the collection
	 * @param collectionType The type of collection to use
	 * @param actionChoice Where to insert the elements in the collection
	 * @return the elapsed time in seconds, as a float
	 */
	public float runTest(Integer amountOfData, COLLECTION_TYPES collectionType, ACTION_CHOICES actionChoice) {
		Collection<SampleData> collection = createCollection(amountOfData, collectionType);

		// Perform the test
		long startTime;
		if (actionChoice == ACTION_CHOICES.Add) {
			SampleData[] data = createData(amountOfData);		
			startTime = System.currentTimeMillis();
			addData(data, collection);
		} else { // Retrieve
			SampleData[] data = createData(1000000);	
			addData(data, collection);
			startTime = System.currentTimeMillis();
			boolean result = findData(amountOfData, data, collection);
			if (!result) throw new IllegalStateException("Test failed!");
		}
		long endTime = System.currentTimeMillis();
		return (endTime - startTime) / 1000.0f;
	}

	private SampleData[] createData(Integer amountOfData) {
		SampleData[] data = new SampleData[amountOfData];
		for (int i = 0; i < amountOfData; i++) data[i] = new SampleData();
		return data;
	}
	
	private Collection<SampleData> createCollection(Integer amountOfData, COLLECTION_TYPES collectionType) {
		// Create an empty collection of the desired type
		Collection<SampleData> collection = null;
		if (collectionType == COLLECTION_TYPES.ArrayList)
			collection = new ArrayList<>();
		else if (collectionType == COLLECTION_TYPES.LinkedList)
			collection = new LinkedList<>();
		else if (collectionType == COLLECTION_TYPES.TreeSet)
			collection = new TreeSet<>();
		else if (collectionType == COLLECTION_TYPES.HashSet)
			collection = new HashSet<>();
		return collection;
	}
	
	private void addData(SampleData[] data, Collection<SampleData> collection) {
		for (SampleData element : data) {
			collection.add(element);
		}
	}
	
	private boolean findData(Integer amountOfData, SampleData[] data, Collection<SampleData> collection) {
		boolean success = true;
		for (int i = 0; i < amountOfData; i++) {
			SampleData objectToFind = data[(int) (Math.random() * data.length)];
			success &= collection.contains(objectToFind);
		}
		return success;
	}
}
