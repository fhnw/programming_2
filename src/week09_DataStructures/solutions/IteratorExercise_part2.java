package week09_DataStructures.solutions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeSet;

import week09_DataStructures.ListTesting.SampleData;

public class IteratorExercise_part2 {

	public static void main(String[] args) {
		// Create objects
		SampleData[] data = fiveRandomObjects();
		
		// ArrayList
		ArrayList<SampleData> arrayList = new ArrayList<>();
		for (SampleData d : data) arrayList.add(d);
		System.out.println("ArrayList\n---------");
		for (SampleData d : arrayList) System.out.println(d.getName());
		
		// TreeSet
		TreeSet<SampleData> treeSet = new TreeSet<>();
		for (SampleData d : data) treeSet.add(d);
		System.out.println("\n\nTreeSet\n-------");
		for (SampleData d : treeSet) System.out.println(d.getName());
		
		// HashMap
		HashMap<Integer, SampleData> hashMap = new HashMap<>();
		for (SampleData d : data) hashMap.put(d.getID(), d);
		System.out.println("\n\nHashMap\n-------");
		for (Integer id : hashMap.keySet()) System.out.println(hashMap.get(id).getName());
	}

	private static SampleData[] fiveRandomObjects() {
		// First, create 100 objects
		SampleData[] baseData = new SampleData[100];
		for (int i = 0; i < baseData.length; i++) baseData[i] = new SampleData();
		
		// Pick five objects at random to return
		SampleData[] data = new SampleData[5];
		for (int i = 0; i < data.length; i++) data[i] = baseData[(int) (Math.random() * 100)];
		
		return data;
	}
	
}
