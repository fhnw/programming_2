package gravityExample;

public class Test {

	public static void main(String[] args) {
		Planet earth = new Planet("Earth", 5.97237E24, 6378137);
		System.out.println("Gravitational acceleration of Earth: " + earth.getGravity() + " m/s²");
		System.out.println("Force in newtons on an 80kg person: " + forceNewtons(earth) + " newtons");
		System.out.println();
		
		Animal marble = new Animal("Marble", Animal.Species.Cat, 6, 20);
		System.out.println("Gravitational acceleration of Marble: " + marble.getGravity() + " m/s²");
		System.out.println("Force in newtons on a 1kg mass: " + forceNewtons(marble) + " newtons");
		System.out.println();
		
		// Gravity is a "functional" interface: it only defines one abstract method.
		//
		// We can use a lambda to create an anonymous class that implements Gravity.
		// We have to provide the content of the getGravity method.
		//
		// Here, we use data for the sun
		double force = forceNewtons(() -> {
			double mass = 1.9885E30;
			double radius = 695700000;
			return Gravity.G * mass / (radius * radius);
        });
		System.out.println("Sun's force in newtons on a 1kg mass: " + force + " newtons");
	}
	
	/**
	 * Calculate the force on a 1kg mass, in newtons
	 */
	private static double forceNewtons(Gravity thing) {
		return thing.getGravity();
	}
}
