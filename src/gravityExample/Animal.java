package gravityExample;

public class Animal implements Comparable<Animal>, Gravity {
	public enum Species { Cat, Dog, Horse };
	
	private final String name;
	private final Species species;
	private double mass; // in kg
	private double height; // in cm
	
	public Animal(String name, Species species, double mass, double height) {
		this.name = name;
		this.species = species;
		this.mass = mass;
		this.height = height;
	}

	/** Force of gravity is mass / radius^2,
	 * where mass is in kg and radius is in meters */
	@Override
	public double getGravity() {
		double radius = height / 200;
		return Gravity.G * mass / (radius * radius);
	}

	@Override
	public int compareTo(Animal a) {
		if (this.mass < a.mass) return -1;
		else if (this.mass > a.mass) return 1;
		else return 0;
	}
	
}
