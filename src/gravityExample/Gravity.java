package gravityExample;

public interface Gravity {
	public final static double G = 6.674E-11;
	
	public double getGravity();
}
