package gravityExample;

public class Planet implements Gravity {
	private final String name;
	private double mass; // kg
	private double radius; // m
	
	public Planet(String name, double mass, double radius) {
		this.name = name;
		this.mass = mass;
		this.radius = radius;
	}

	@Override
	public double getGravity() {
		return Gravity.G * mass / (radius * radius);
	}

}
