package week05_Files_IO.FileAccessDemo;

public class FileAccessDemo_Controller {
	private final FileAccessDemo_View view;
	private final FileAccessDemo_Model model;
	
	public FileAccessDemo_Controller(FileAccessDemo_View view, FileAccessDemo_Model model) {
		this.view = view;
		this.model = model;
		
		view.btnSave.setOnAction( event -> {
			String in = view.txtTextEntry.getText();
			model.setSaveFile(in);
			view.updateDisplay();
		});
	}
}
