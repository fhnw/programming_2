package week05_Files_IO.FileAccessDemo;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class FileAccessDemo_View {
	private final FileAccessDemo_Model model;
	private final Stage stage;
	
	// GUI controls
	Label lblTextEntry = new Label("Enter some text here");
	TextField txtTextEntry = new TextField();
	Button btnSave = new Button("Save");

	Label lblSaveFileContents = new Label();
	Label lblThisPackageContents = new Label();
	Label lblOtherPackageContents = new Label();
	
	public FileAccessDemo_View(Stage stage, FileAccessDemo_Model model) {
		this.stage = stage;
		this.model = model;
		
		
		HBox top = new HBox(lblTextEntry, txtTextEntry, btnSave);
		top.getStyleClass().add("hbox");
		
		VBox root = new VBox(top, lblSaveFileContents, lblThisPackageContents, lblOtherPackageContents);
		root.getStyleClass().add("vbox");
		
		Scene scene = new Scene(root);
		stage.setScene(scene);
		scene.getStylesheets().add(
				getClass().getResource("FileAccess.css").toExternalForm());
		stage.setTitle("File access demo");
	}
	
	public void start() {
		updateDisplay();
		stage.show();
	}

	public void updateDisplay() {
		lblSaveFileContents.setText(model.getSaveFile());
		lblThisPackageContents.setText(model.getPackageFile());
		lblOtherPackageContents.setText(model.getOtherPackageFile());
	}
}
