package week05_Files_IO.FileAccessDemo;

import javafx.application.Application;
import javafx.stage.Stage;

public class FileAccessDemo extends Application {

	private FileAccessDemo_View view;
	private FileAccessDemo_Controller controller;
	private FileAccessDemo_Model model;
	
	public static void main(String[] args) {
		launch();
	}

	@Override
	public void init() {
		// Initialization - but note that model does not yet exist!
	}
	
	@Override
	public void start(Stage stage) throws Exception {
		this.model = new FileAccessDemo_Model();
		this.view = new FileAccessDemo_View(stage, model);
		this.controller = new FileAccessDemo_Controller(view, model);

		model.readSaveFile();		
		view.start();
	}
	
	@Override
	public void stop() {
		model.writeSaveFile();
	}

}
