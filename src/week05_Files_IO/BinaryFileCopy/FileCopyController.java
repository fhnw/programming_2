package week05_Files_IO.BinaryFileCopy;

import java.io.File;

import javafx.stage.FileChooser;

public class FileCopyController {
	private final FileCopyView view;
	private final FileCopyModel model;
	private File inputFile = null;
	private File outputFile = null;
	
	public FileCopyController(FileCopyView view, FileCopyModel model) {
		this.view = view;
		this.model = model;
		
		view.btnInput.setOnAction(event -> {
			FileChooser fileChooser = new FileChooser();
			 fileChooser.setTitle("File to copy");
			 inputFile = fileChooser.showOpenDialog(view.getStage());
			 if (inputFile != null) view.txtInput.setText(inputFile.toString());
		 });
		
		view.btnOutput.setOnAction(event -> {
			FileChooser fileChooser = new FileChooser();
			 fileChooser.setTitle("Location of copy");
			 outputFile = fileChooser.showSaveDialog(view.getStage());
			 if (outputFile != null) view.txtOutput.setText(outputFile.toString());
		 });
		
		view.btnCopy.setOnAction( event -> {
			model.FileCopy(inputFile, outputFile);
		});
	}
}
