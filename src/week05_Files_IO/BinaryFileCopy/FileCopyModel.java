package week05_Files_IO.BinaryFileCopy;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import javafx.scene.control.Alert;

public class FileCopyModel {

	public FileCopyModel() {
	}

	public void FileCopy(File fileIn, File fileOut) {
		// Check that fileIn is ok
		boolean fileInOk = fileIn.exists() && !fileIn.isDirectory() || fileIn.canRead();
		if (!fileInOk) {
			Alert alert = new Alert(Alert.AlertType.ERROR, "Problem with input file");
			alert.showAndWait();
		}
		
		// Check that fileOut is ok
		boolean fileOutOk = true;
		try {
			fileOut.createNewFile(); // Create file if it does not exist
			fileOutOk = fileOut.exists() && !fileOut.isDirectory() && fileOut.canWrite();
		} catch( IOException e ) {
			fileOutOk = false;
		}
		if (!fileOutOk) {
			Alert alert = new Alert(Alert.AlertType.ERROR, "Problem with output file");
			alert.showAndWait();
		}
		
		// If everything is ok, copy the file
		if (fileInOk && fileOutOk) {		
			try (	FileInputStream streamIn = new FileInputStream(fileIn);
					FileOutputStream streamOut = new FileOutputStream(fileOut); ) {
				byte[] buffer = new byte[4096];
				int bytesRead;
				// In a loop: read, write, read, write - until done
				bytesRead = streamIn.read(buffer); // returns actual number of bytes
				while (bytesRead != -1) {
					streamOut.write(buffer, 0, bytesRead); // buffer, begin, length
					bytesRead = streamIn.read(buffer);
				}
				streamOut.flush(); // Ensure all data has actually been written
			} catch (Exception e) {
				Alert alert = new Alert(Alert.AlertType.ERROR, e.toString());
				alert.showAndWait();
			}
		}
	}
}
