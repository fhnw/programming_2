package week05_Files_IO.BinaryFileCopy;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.stage.Stage;

public class FileCopyView {
	private final FileCopyModel model;
	private final Stage stage;
	
	// GUI controls
	Label lblInstructions = new Label("Enter complete paths for the input and output files");
	Label lblInput = new Label("Input file");
	TextField txtInput = new TextField();
	Label lblOutput = new Label("Output file");
	TextField txtOutput = new TextField();
	Button btnInput = new Button("Choose");
	Button btnOutput = new Button("Choose");
	Button btnCopy = new Button("Copy");
	
	public FileCopyView(Stage stage, FileCopyModel model) {
		this.stage = stage;
		this.model = model;
		
		GridPane root = new GridPane();
		root.getStyleClass().add("gridpane");
		
		// Some formatting is (unfortunately) best done here, instead of CSS
		ColumnConstraints col1 = new ColumnConstraints();
	    ColumnConstraints col2 = new ColumnConstraints();
	    ColumnConstraints col3 = new ColumnConstraints();
	    col3.setHgrow( Priority.ALWAYS );
	    root.getColumnConstraints().addAll( col1, col2, col3 );

		root.add(lblInstructions,  0,  0, 3, 1);
		root.add(lblInput, 0, 1);
		root.add(btnInput, 1, 1);
		root.add(txtInput, 2, 1);
		root.add(lblOutput, 0, 2);
		root.add(btnOutput, 1, 2);
		root.add(txtOutput, 2, 2);
		root.add(btnCopy, 0, 3, 3, 1);

		// Button has large max-width; tell GridPane to expand it.
		btnCopy.setMaxWidth(10000);
		GridPane.setFillWidth(btnCopy,  true);
		
		Scene scene = new Scene(root);
		scene.getStylesheets().add(
				getClass().getResource("FileCopy.css").toExternalForm());
		stage.setScene(scene);
		stage.setTitle("File copy");
	}
	
	public Stage getStage() {
		return stage;
	}
	
	public void start() {
		stage.show();
	}
	
}
