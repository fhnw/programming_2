package week05_Files_IO.FileWithPets;

public class Pet {
	private static int NextID = 0;
	
	private int ID;
	private String name;
	
	public Pet() {
		this.ID = NextID++;
	}
	
	/**
	 * Restricted access to special constructor than can set the ID
	 */
	Pet(int ID) {
		this.ID = ID;
	}
	
	// Getters and Setters
	
	public int getID() {
		return ID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
