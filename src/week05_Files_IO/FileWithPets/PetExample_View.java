package week05_Files_IO.FileWithPets;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class PetExample_View {
	private final PetExample_Model model;
	private final Stage stage;
	
	// GUI controls
	Button btnLoad = new Button("Load pets");
	Button btnSave = new Button("Save pets");
	Label lblNumPets = new Label();
	Label lblInfo = new Label();

	public PetExample_View(Stage stage, PetExample_Model model) {
		this.stage = stage;
		this.model = model;
		
		HBox buttons = new HBox(btnLoad, btnSave);
		buttons.getStyleClass().add("hbox");
		
		VBox root = new VBox(lblNumPets, lblInfo, buttons);
		root.getStyleClass().add("vbox");
		
		Scene scene = new Scene(root);
		stage.setScene(scene);
		scene.getStylesheets().add(
				getClass().getResource("PetExample.css").toExternalForm());
		stage.setTitle("Pet Example");
	}
	
	public void start() {
		stage.show();
	}
}
