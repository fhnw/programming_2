package week05_Files_IO.FileWithPets;

import java.util.ArrayList;

public class PetExample_Controller {
	private final PetExample_View view;
	private final PetExample_Model model;
	
	public PetExample_Controller(PetExample_View view, PetExample_Model model) {
		this.view = view;
		this.model = model;
		
		view.btnLoad.setOnAction( event -> {
			model.loadPets();
			view.lblNumPets.setText(model.getPets().size() + " pets");
			view.lblInfo.setText("Pets loaded");
		});
		
		view.btnSave.setOnAction( event -> {
			model.savePets();
			view.lblInfo.setText("Pets saved");
		});
	}
}
