package week05_Files_IO.FileWithPets;

import javafx.application.Application;
import javafx.stage.Stage;

public class PetExample extends Application {

	private PetExample_View view;
	private PetExample_Controller controller;
	private PetExample_Model model;
	
	public static void main(String[] args) {
		launch();
	}

	@Override
	public void init() {
		// Initialization - but note that model does not yet exist!
	}
	
	@Override
	public void start(Stage stage) throws Exception {
		this.model = new PetExample_Model();
		this.view = new PetExample_View(stage, model);
		this.controller = new PetExample_Controller(view, model);
		view.start();
	}
}
