package week05_Files_IO.FileExercise_AddingNumbers;

import java.util.ArrayList;

/**
 * Our model only contains a bunch of strings
 */
public class FileExercise_Model {
	private static String NUMBERS_FILE = "numbers.txt";

	private ArrayList<Integer> numbers = null;

	public void readFile() {
		// TODO: Complete this method
		numbers = null;
	}

	public boolean hasNumbers() {
		return (numbers != null);
	}
	
	public ArrayList<Integer> getNumbers() {
		return numbers;
	}
	
	public int calculateTotal() {
		int total = 0;
		for (int number : numbers) total += number;
		return total;
	}
}
