package xtras.sqliteDemo;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SQLiteDemoApp {
	
	// SQLite demo project based on: https://www.youtube.com/watch?v=JPsWaI5Z3gs

	public static void main(String[] args) throws ClassNotFoundException, SQLException{
		SQLiteDemoMethods demo =   new SQLiteDemoMethods ();
		ResultSet rs;
		
		try {
			rs = demo.displayUsers();
			while(rs.next()) {
				
				//prints results for each user
				System.out.println(rs.getString("fname")  + " " + rs.getString("lname"));
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

}
