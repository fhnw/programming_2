package xtras.sqliteDemo;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class SQLiteDemoMethods {
	private static Connection con;
	private static boolean hasData =false;
	
	// returns all users from user table as resultset
	public ResultSet displayUsers() throws ClassNotFoundException, SQLException {
		if(con == null) {
			this.getConnection();
		}
		
		Statement state = con.createStatement();
		ResultSet res = state.executeQuery("SELECT fname, lname FROM user");
		return res;
	}
	
	// connect to database
	private void getConnection() throws ClassNotFoundException, SQLException {
		Class.forName("org.sqlite.JDBC");
		this.con = DriverManager.getConnection("jdbc:sqlite:SQLiteDemo.db");
		this.initialise();	
	}
	
	// creates user table with initial data
	private void initialise() throws SQLException {
		if(!this.hasData) {
			
			this.hasData = true;
			Statement state = con.createStatement();
			
			//looks for table user
			ResultSet res = state.executeQuery("SELECT name FROM sqlite_master WHERE type ='table' AND name= 'user'");
			
			// creates table user when doesn't exist
			if(!res.next()) {
				System.out.println("Building user table...");
				Statement  state2 = con.createStatement();
				state2.execute("CREATE TABLE user(id integer primary key,"
						+ "fname varchar(50), lname varchar(55)" +
						");");
				
				// filling initial data into user table
				PreparedStatement prep = con.prepareStatement("INSERT INTO user "
						+"values(?,?,?);"
						);
				
				//creating user with fname = John and lname = Neil
				prep.setString(2, "John");
				prep.setString(3, "Neil");
				
				prep.execute();
				
				//creating user with fname = Paul and lname = Smith
				PreparedStatement prep2 = con.prepareStatement("INSERT INTO user "
						+"values(?,?,?);"
						);
				prep2.setString(2, "Paul");
				prep2.setString(3, "Smith");
				
				prep2.execute();
				
			}
		}
		
		
	}
	
	// method to add new users to user table
	public void addUser(String firstname, String lastName) throws ClassNotFoundException, SQLException{
		
		// check for connection
		if(con == null) {
			this.getConnection();
		}
		
		//create new user
		PreparedStatement prep = con.prepareStatement ("INSERT INTO user "
				+"VALUES (?,?,?);"
				);
		prep.setString(2, firstname);
		prep.setString(3, lastName);
		prep.execute();
		
	}
	
	//delete user 
	public void deleteUser(String firstname, String lastname) throws ClassNotFoundException, SQLException{
		if(con == null) {
			this.getConnection();
		}
		
		PreparedStatement prep = con.prepareStatement ("DELETE FROM user  WHERE fname=" + "'" + firstname+ "' AND " +  "lname=" 
														+ "'" + lastname + "';"
				
				);
		
		prep.execute();
	}
	
	//update method for firstname
	
	public void updateFirstname(String newFirstname, String oldFirstname) throws ClassNotFoundException, SQLException{
		if(con == null) {
			this.getConnection();
		}
		
		PreparedStatement prep = con.prepareStatement ("UPDATE 'user' SET fname=" + "'" + newFirstname + "'" + 
		" WHERE fname ='" +oldFirstname+"';"
				);
		prep.execute();
	}
	
	
	

}
