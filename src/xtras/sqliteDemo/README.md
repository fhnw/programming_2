Ursprüngliche Autoren, FS 2021

Nicolas Dubuis & Simon von Sprecher

**--Additional steps to run the project --**

1.) Download and save the SQLite JDBC Driver. Current home: https://github.com/xerial/sqlite-jdbc
    Here is the directory containing the various releases. Pick the newest release, then select
    the file sqlite-jdbc-xxxxx.jar Save this file in a location you will remember.

2.) Right click the cloned project in eclipse go to
    _Build Path_ / _Libraries_  and click on _Modulepath_

3.) Click on _Add External JARs_  select the  SQLite JAR file then  _Apply and Close_

4.) Now you can run the project!
