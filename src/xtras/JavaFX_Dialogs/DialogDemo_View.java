package xtras.JavaFX_Dialogs;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class DialogDemo_View {
	private Stage stage;
	private DialogDemo_Model model;
	
    protected Button btnAlertInfo = new Button("Alert: info");
    protected Button btnAlertConfirmation = new Button("Alert: confirm");
    protected Button btnAlertError = new Button("Alert: error");
    protected Button btnAlertWarning = new Button("Alert: warning");
    protected Button btnAlertNone = new Button("Alert: none");
    
    protected Button btnChoiceDialog = new Button("Choice dialog");
    protected Button btnTextInput = new Button("Text input dialog");
    protected Button btnCustomDialog = new Button("Custom dialog");
    protected Label lblResult = new Label();

    public DialogDemo_View(Stage stage, DialogDemo_Model model) {
    	this.stage = stage;
    	this.model = model;
    	
	    GridPane root = new GridPane();
	    
	    root.add(btnAlertInfo, 0, 0);

	    root.add(btnAlertConfirmation, 1, 0);

	    root.add(btnAlertError, 2, 0);
	    
	    root.add(btnAlertWarning, 3, 0);

	    root.add(btnAlertNone, 4, 0);

	    root.add(btnChoiceDialog, 0, 1);

	    root.add(btnTextInput, 1, 1);

	    root.add(btnCustomDialog, 2, 1);
	    
	    // Label for showing results
	    root.add(lblResult, 0, 2, 6, 1);
	    
	    // Create the scene using our layout; then display it
	    Scene scene = new Scene(root);
	    stage.setTitle("Dialog Demo");
	    stage.setScene(scene);
    }
    
    public void start() {
    	stage.show();
    }
}
