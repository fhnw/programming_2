package xtras.JavaFX_Dialogs;

import javafx.application.Application;
import javafx.stage.Stage;

/**
 * Demonstration of the Dialog class available beginning with Java 8
 * 
 * Note: The results are returned within an object of the class Optional, which will contain either
 * null (if the dialog was cancelled, or closed using the frame button), or else a result of the
 * desired type.
 */
public class DialogDemo extends Application {

	public static void main(String[] args) {
		launch();
	}

	@Override
	public void start(Stage stage) {
		DialogDemo_Model model = new DialogDemo_Model();
		DialogDemo_View view = new DialogDemo_View(stage, model);
		DialogDemo_Controller controller = new DialogDemo_Controller(view, model);
		
		view.start();
	}
}