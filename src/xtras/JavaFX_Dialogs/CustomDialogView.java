package xtras.JavaFX_Dialogs;

import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

public class CustomDialogView extends GridPane {
    protected Label lblPet = new Label("Pet");
    protected TextField txtPet = new TextField();
    protected Label lblOwner = new Label("Owner");
    protected TextField txtOwner = new TextField();

	public CustomDialogView() {
		super();
        this.add(lblPet, 0, 0);
        this.add(txtPet, 1, 0);
        this.add(lblOwner, 0, 1);
        this.add(txtOwner, 1, 1);		
	}
}
