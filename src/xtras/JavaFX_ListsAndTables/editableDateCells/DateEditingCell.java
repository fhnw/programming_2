package xtras.JavaFX_ListsAndTables.editableDateCells;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javafx.scene.control.DatePicker;
import javafx.scene.control.TableCell;
import javafx.util.StringConverter;

class DateEditingCell extends TableCell<Person, LocalDate> {
	private static final String dtPattern = "yyyy-MMM-dd";
	private static final DateTimeFormatter dtFormat = DateTimeFormatter.ofPattern(dtPattern);
	private DatePicker datePicker;

	public DateEditingCell() {
	}

	@Override
	public void startEdit() {
		if (!isEmpty()) {
			super.startEdit();
			createDatePicker();
			setText(null);
			setGraphic(datePicker);
		}
	}

	@Override
	public void cancelEdit() {
		super.cancelEdit();

		setText(getDate().toString());
		setGraphic(null);
	}

	@Override
	public void updateItem(LocalDate item, boolean empty) {
		super.updateItem(item, empty);

		if (empty) {
			setText(null);
			setGraphic(null);
		} else {
			if (isEditing()) {
				if (datePicker != null) {
					
					datePicker.setValue(getDate());
				}
				setText(null);
				setGraphic(datePicker);
			} else {
				setText(getDate().format(dtFormat));
				setGraphic(null);
			}
		}
	}

	private void createDatePicker() {
		datePicker = new DatePicker(getDate());
		datePicker.setMinWidth(this.getWidth() - this.getGraphicTextGap() * 2);
		datePicker.setPromptText(dtPattern);
		datePicker.setConverter(new dtConverter(dtFormat)); // See private class below
		datePicker.setOnAction((e) -> {
			System.out.println("Committed: " + datePicker.getValue().toString());
			commitEdit(datePicker.getValue());
		});
	}

	private LocalDate getDate() {
		return getItem() == null ? LocalDate.now()
				: getItem();
	}

	// Changing the format used by the DatePicker is...difficult.
	// See the DatePicker documentation for more information
	private static class dtConverter extends StringConverter<LocalDate> {
	     String pattern = "yyyy-MM-dd";
	     DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

	     public dtConverter(DateTimeFormatter dateFormatter) {
	    	 this.dateFormatter = dateFormatter;	    	 
	     }

	     @Override public String toString(LocalDate date) {
	         if (date != null) {
	             return dateFormatter.format(date);
	         } else {
	             return "";
	         }
	     }

	     @Override public LocalDate fromString(String string) {
	         if (string != null && !string.isEmpty()) {
	             return LocalDate.parse(string, dateFormatter);
	         } else {
	             return null;
	         }
	     }
	 }
}
