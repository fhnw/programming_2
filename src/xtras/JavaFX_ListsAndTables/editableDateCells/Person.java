package xtras.JavaFX_ListsAndTables.editableDateCells;

import java.time.LocalDate;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Person {

	private final SimpleStringProperty firstName;
	private final SimpleObjectProperty<Animal> typ;
	private final SimpleObjectProperty<LocalDate> birthday;

	public Person(String firstName, Animal typ, LocalDate birthday) {
		this.firstName = new SimpleStringProperty(firstName);
		this.typ = new SimpleObjectProperty<>(typ);
		this.birthday = new SimpleObjectProperty<>(birthday);
	}

	public String getFirstName() {
		return firstName.get();
	}

	public StringProperty firstNameProperty() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName.set(firstName);
	}

	public Animal getTypObj() {
		return typ.get();
	}

	public ObjectProperty<Animal> typObjProperty() {
		return this.typ;
	}

	public void setTypObj(Animal typ) {
		this.typ.set(typ);
	}

	public LocalDate getBirthday() {
		return birthday.get();
	}

	public ObjectProperty<LocalDate> birthdayProperty() {
		return this.birthday;
	}

	public void setBirthday(LocalDate birthday) {
		this.birthday.set(birthday);
	}

}

