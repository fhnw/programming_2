package xtras.JavaFX_ListsAndTables.editableDateCells;

import java.time.LocalDate;

import javafx.application.Application;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;

/**
 * Example copied from https://gist.github.com/haisi/0a82e17daf586c9bab52
 * Original author: Hasan Selman Kara
 * Adapted by: Brad Richards (FHNW)
 */
public class AddressBook extends Application {

	private TableView<Person> table = new TableView<>();
	private final ObservableList<Animal> typData = FXCollections.observableArrayList(new Animal("Dog"), new Animal("Cat"),
			new Animal("Horse"));
	private final ObservableList<Person> data = FXCollections.observableArrayList(
			new Person("Jacob", typData.get(0), LocalDate.now()), new Person("Urs", typData.get(1), LocalDate.now()),
			new Person("Hans", typData.get(2), LocalDate.now()), new Person("Ueli", typData.get(2), LocalDate.now()));

	final HBox hb = new HBox();

	@Override
	public void start(Stage stage) {
		Scene scene = new Scene(new Group());
		stage.setWidth(550);
		stage.setHeight(550);

		final Label label = new Label("Address Book");
		label.setFont(new Font("Arial", 20));

		table.setEditable(true);

		TableColumn<Person, String> firstNameCol = new TableColumn<>("Vorname");
		firstNameCol.setMinWidth(100);
		firstNameCol.setCellValueFactory(cellData -> cellData.getValue().firstNameProperty());
		firstNameCol.setCellFactory(c -> new TextEditingCell());
		firstNameCol.setOnEditCommit((TableColumn.CellEditEvent<Person, String> t) -> {
			((Person) t.getTableView().getItems().get(t.getTablePosition().getRow())).setFirstName(t.getNewValue());
		});

		TableColumn<Person, Animal> lastNameCol = new TableColumn<>("Pet");
		lastNameCol.setMinWidth(100);
		lastNameCol.setCellValueFactory(cellData -> cellData.getValue().typObjProperty());
		lastNameCol.setCellFactory(c -> new ComboBoxEditingCell(typData));
		lastNameCol.setOnEditCommit((TableColumn.CellEditEvent<Person, Animal> t) -> {
			((Person) t.getTableView().getItems().get(t.getTablePosition().getRow())).setTypObj(t.getNewValue());
		});

		TableColumn<Person, LocalDate> emailCol = new TableColumn<>("Birthday");
		emailCol.setMinWidth(200);
		emailCol.setCellValueFactory(cellData -> cellData.getValue().birthdayProperty());
		emailCol.setCellFactory(c -> new DateEditingCell());
		emailCol.setOnEditCommit((TableColumn.CellEditEvent<Person, LocalDate> t) -> {
			((Person) t.getTableView().getItems().get(t.getTablePosition().getRow())).setBirthday(t.getNewValue());
		});

		table.setItems(data);
		table.getColumns().addAll(firstNameCol, lastNameCol, emailCol);

		final TextField addFirstName = new TextField();
		addFirstName.setPromptText("First Name");
		addFirstName.setMaxWidth(firstNameCol.getPrefWidth());

		final TextField addLastName = new TextField();
		addLastName.setPromptText("Last Name");
		addLastName.setMaxWidth(lastNameCol.getPrefWidth());

		final TextField addEmail = new TextField();
		addEmail.setPromptText("email");
		addEmail.setMaxWidth(emailCol.getPrefWidth());

		final Button addButton = new Button("Add");
		addButton.setOnAction((ActionEvent e) -> {
			data.add(new Person(addFirstName.getText(), new Animal("Hund"), LocalDate.now()));
			addFirstName.clear();
			addLastName.clear();
			addEmail.clear();
		});

		hb.getChildren().addAll(addFirstName, addLastName, addEmail, addButton);
		hb.setSpacing(3);

		final VBox vbox = new VBox();
		vbox.setSpacing(5);
		vbox.setPadding(new Insets(10, 0, 0, 10));
		vbox.getChildren().addAll(label, table, hb);

		((Group) scene.getRoot()).getChildren().addAll(vbox);

		stage.setScene(scene);
		stage.setTitle("Address Book");
		stage.show();
	}

	/**
	 * The main method...
	 */
	public static void main(String[] args) {
		launch(args);
	}

}