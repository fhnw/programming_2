package xtras.JavaFX_ListsAndTables.editableDateCells;

import javafx.collections.ObservableList;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.TableCell;

class ComboBoxEditingCell extends TableCell<Person, Animal> {

	private ComboBox<Animal> comboBox;
	ObservableList<Animal> animalData;

	public ComboBoxEditingCell(ObservableList<Animal> animalData) {
		this.animalData = animalData;
	}

	@Override
	public void startEdit() {
		if (!isEmpty()) {
			super.startEdit();
			createComboBox();
			setText(null);
			setGraphic(comboBox);
		}
	}

	@Override
	public void cancelEdit() {
		super.cancelEdit();

		setText(getAnimal().getAnimal());
		setGraphic(null);
	}

	@Override
	public void updateItem(Animal item, boolean empty) {
		super.updateItem(item, empty);

		if (empty) {
			setText(null);
			setGraphic(null);
		} else {
			if (isEditing()) {
				if (comboBox != null) {
					comboBox.setValue(getAnimal());
				}
				setText(getAnimal().getAnimal());
				setGraphic(comboBox);
			} else {
				setText(getAnimal().getAnimal());
				setGraphic(null);
			}
		}
	}

	private void createComboBox() {
		comboBox = new ComboBox<>(animalData);
		comboBoxConverter(comboBox);
		comboBox.valueProperty().set(getAnimal());
		comboBox.setMinWidth(this.getWidth() - this.getGraphicTextGap() * 2);
		comboBox.setOnAction((e) -> {
			System.out.println("Committed: " + comboBox.getSelectionModel().getSelectedItem());
			commitEdit(comboBox.getSelectionModel().getSelectedItem());
		});
		// comboBox.focusedProperty().addListener((ObservableValue<? extends Boolean>
		// observable, Boolean oldValue, Boolean newValue) -> {
		// if (!newValue) {
		// commitEdit(comboBox.getSelectionModel().getSelectedItem());
		// }
		// });
	}

	private void comboBoxConverter(ComboBox<Animal> comboBox) {
		// Define rendering of the list of values in ComboBox drop down.
		comboBox.setCellFactory((c) -> {
			return new ListCell<Animal>() {
				@Override
				protected void updateItem(Animal item, boolean empty) {
					super.updateItem(item, empty);

					if (item == null || empty) {
						setText(null);
					} else {
						setText(item.getAnimal());
					}
				}
			};
		});
	}

	private Animal getAnimal() {
		return getItem() == null ? new Animal("") : getItem();
	}
}

