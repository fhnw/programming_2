package xtras.JavaFX_ListsAndTables.editableDateCells;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Animal {

	private final SimpleStringProperty animal;

	public Animal(String animal) {
		this.animal = new SimpleStringProperty(animal);
	}

	public String getAnimal() {
		return this.animal.get();
	}

	public StringProperty animalProperty() {
		return this.animal;
	}

	public void setAnimal(String animal) {
		this.animal.set(animal);
	}

	@Override
	public String toString() {
		return animal.get();
	}

}

