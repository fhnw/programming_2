package xtras.JavaFX_ListsAndTables.nationalAssemblyElection;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class NA_Model {
	private static final String FILE_NAME = "NationalAssemblyElections2007.txt";
	private static final String TAB = "\\t";

	private final ObservableList<Result> results = FXCollections.observableArrayList();

	public NA_Model() {
		InputStream inStream = this.getClass().getResourceAsStream(FILE_NAME);
		try (BufferedReader fileIn = new BufferedReader(new InputStreamReader(inStream))) {
			String line = fileIn.readLine(); // Skip header line
			line = fileIn.readLine();
			while (line != null) {
				results.add(new Result(line.split(TAB)));
				line = fileIn.readLine();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// Getters and setters

	public ObservableList<Result> getResults() {
		return results;
	}
}
