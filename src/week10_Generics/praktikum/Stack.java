package week10_Generics.praktikum;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class Stack<T> implements Iterable<T> {
	private StackElement<T> head = null;

	private class StackElement<T> {
		T data;
		StackElement<T> next;
	}

	public T pop() {
		T data = null;
		if (head != null) {
			data = head.data;
			head = head.next;
		}
		return data;
	}

	public void push(T data) {
		StackElement<T> newElt = new StackElement();
		newElt.data = data;
		newElt.next = head;
		head = newElt;
	}

	public int size() {
		int count = 0;
		StackElement<T> cursor = head;
		while (cursor != null) {
			cursor = cursor.next;
			count++;
		}
		return count;
	}

	public boolean contains(T data) {
		boolean found = false;
		StackElement<T> cursor = head;
		while (!found && cursor != null) {
			found = cursor.data.equals(data);
			cursor = cursor.next;
		}
		return found;
	}

	@Override
	public Iterator<T> iterator() {
		return new StackIterator();
	}

	/**
	 * In a stack, you normally can only access the top element. Perhaps we should
	 * not provide an Iterator.
	 * 
	 * Note that we are not required to implement any other operations. For example,
	 * "remove" will (be default) thrown an UnsupportedOperationException, which is
	 * correct, as we cannot remove elements from the middle of a stack.
	 */
	public class StackIterator implements Iterator<T> {
		private StackElement<T> cursor = null;

		private StackIterator() {
			cursor = head;
		}

		@Override
		public boolean hasNext() {
			return (cursor != null);
		}

		@Override
		public T next() {
			T data = null;
			if (cursor != null) {
				data = cursor.data;
				cursor = cursor.next;
			} else {
				throw new NoSuchElementException();
			}
			return data;
		}
	}
}
