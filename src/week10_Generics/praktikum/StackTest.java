package week10_Generics.praktikum;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Iterator;
import java.util.NoSuchElementException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class StackTest {
	private Stack<String> names;

	@BeforeEach
	void setUp() throws Exception {
		names = new Stack<>();
	}

	@Test
	void testNormal() {
		assertEquals(0, names.size());
		names.push("Anna");
		names.push("Thomas");
		assertEquals(2, names.size());
		assertEquals("Thomas", names.pop());
		assertEquals("Anna", names.pop());
		assertEquals(0, names.size());
	}
	
	@Test
	void testException() {
		try {
			names.pop();
		} catch(NoSuchElementException e) {
			assertTrue(true);
		} catch(Exception e) {
			fail();
		}
	}

	@Test
	void testIterator() {
		names.push("Anna");
		names.push("Thomas");
		Iterator<String> it = names.iterator();
		assertTrue(it.hasNext());
		assertEquals("Thomas", it.next());
		assertTrue(it.hasNext());
		assertEquals("Anna", it.next());
		assertFalse(it.hasNext());
	}
}
