package week12_Dates_Times.solution;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collections;

public class Room {
  public static final LocalTime START = LocalTime.of(8, 0, 0);
  public static final LocalTime END = LocalTime.of(22, 0, 0);

  private final int roomNumber;
  private int capacity;
  private ArrayList<Reservation> reservations = new ArrayList<>();

  public static class Reservation implements Comparable<Reservation> {
    public LocalDate date;
    public LocalTime startTime;
    public int hours;
    
    @Override
    public int compareTo(Reservation o) {
      int result = this.date.compareTo(o.date);
      if (result != 0)
        return result;
      else
        return this.startTime.compareTo(o.startTime);
    }
  }

  public Room(int roomNumber, int capacity) {
    this.roomNumber = roomNumber;
    this.capacity = capacity;
  }

  /**
   * Add a reservation to the list.
   */
  public void addReservation(Reservation res) {
    reservations.add(res);
    Collections.sort(reservations);
  }

  public LocalTime reserve(LocalDate date, int hours) {
    LocalTime startTime = null; // Pessimist - we won't find a time

    // Extract all reservations for the desired date into a new list
    ArrayList<Reservation> res = new ArrayList<>();
    for (Reservation r : reservations) {
      if (r.date.equals(date)) res.add(r);
    }

    if (res.size() == 0) {
      startTime = START; // No reservations, so first possible time
    } else {
      Duration desired = Duration.ofHours(hours);
      // Check before the first reservation
      Duration d = Duration.between(START, res.get(0).startTime);
      if (d.compareTo(desired) >= 0) {
        startTime = START;
      } else { // Check between reservations
        boolean found = false;
        for (int i = 0; !found && i < res.size() - 1; i++) {
          Reservation r1 = res.get(i);
          Reservation r2 = res.get(i + 1);
          Duration diff = Duration.between(r1.startTime, r2.startTime);
          d = diff.minus(Duration.ofHours(r1.hours));
          if (d.compareTo(desired) >= 0) {
            startTime = r1.startTime.plusHours(r1.hours);
            found = true;
          }
        }
        if (!found) { // Check after last reservation
          Reservation lastRes = res.get(res.size() - 1);
          LocalTime endLastReservation = lastRes.startTime.plusHours(lastRes.hours);
          d = Duration.between(endLastReservation, END);
          if (d.compareTo(desired) >= 0) startTime = endLastReservation;
        }
      }
    }
    return startTime;
  }
}
