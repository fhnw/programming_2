package week12_Dates_Times.exercise;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class RoomTest {
  private static final Room testRoom = new Room(123, 20);

  @BeforeAll
  static void setUpBeforeClass() throws Exception {
    // Define test reservations for the following test cases
    testRoom.addReservation(createReservation(2022, 1, 1, 9, 2));
    testRoom.addReservation(createReservation(2022, 1, 1, 12, 1));
    testRoom.addReservation(createReservation(2022, 1, 1, 15, 2));

    testRoom.addReservation(createReservation(2022, 1, 2, 8, 1));
    testRoom.addReservation(createReservation(2022, 1, 2, 9, 2));
    testRoom.addReservation(createReservation(2022, 1, 2, 11, 2));
    testRoom.addReservation(createReservation(2022, 1, 2, 14, 1));
    testRoom.addReservation(createReservation(2022, 1, 2, 16, 1));
    testRoom.addReservation(createReservation(2022, 1, 2, 20, 2));
    
    testRoom.addReservation(createReservation(2022, 1, 4, 9, 2));
    
    testRoom.addReservation(createReservation(2022, 1, 5, 11, 2));
    testRoom.addReservation(createReservation(2022, 1, 5, 14, 2));
  }

  private static Room.Reservation createReservation(int year, int month, int day, int hour, int hours) {
    Room.Reservation res = new Room.Reservation();
    res.date = LocalDate.of(year, month, day);
    res.startTime = LocalTime.of(hour, 0);
    res.hours = hours;
    return res;
  }

  @Test
  void test1() {
    // These test cases find a value before the first reservation
    assertEquals(testRoom.reserve(LocalDate.of(2022, 1, 1), 1), LocalTime.of(8, 0));
    assertEquals(testRoom.reserve(LocalDate.of(2022, 1, 4), 1), LocalTime.of(8, 0));
    assertEquals(testRoom.reserve(LocalDate.of(2022, 1, 5), 2), LocalTime.of(8, 0));
  }

  @Test
  void test2() {
    // These test cases find a value between reservations
    assertEquals(testRoom.reserve(LocalDate.of(2022, 1, 1), 2), LocalTime.of(13, 0));
    assertEquals(testRoom.reserve(LocalDate.of(2022, 1, 2), 2), LocalTime.of(17, 0));
  }

  @Test
  void test3() {
    // These test cases find a value after the last reservation
    assertEquals(testRoom.reserve(LocalDate.of(2022, 1, 1), 3), LocalTime.of(17, 0));
    assertEquals(testRoom.reserve(LocalDate.of(2022, 1, 5), 4), LocalTime.of(16, 0));
  }

  @Test
  void test4() {
    // These test cases find no possible reservation
    assertEquals(testRoom.reserve(LocalDate.of(2022, 1, 1), 6), null);
    assertEquals(testRoom.reserve(LocalDate.of(2022, 1, 2), 4), null);
    assertEquals(testRoom.reserve(LocalDate.of(2022, 1, 5), 7), null);
  }
  
  @Test
  void test5() {
    // This test case is a day when there are no reservations at all
    assertEquals(testRoom.reserve(LocalDate.of(2022, 1, 3), 2), LocalTime.of(8, 0));
  }
  
  @Test
  void test6() {
    // The entire reservations list for this room is empty!
    Room testRoom2 = new Room(124, 20);
    assertEquals(testRoom2.reserve(LocalDate.of(2022, 1, 3), 2), LocalTime.of(8, 0));
  }
}
