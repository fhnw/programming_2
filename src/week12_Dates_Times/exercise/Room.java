package week12_Dates_Times.exercise;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collections;

public class Room {
  public static final LocalTime START = LocalTime.of(8, 0, 0);
  public static final LocalTime END = LocalTime.of(22, 0, 0);

  private final int roomNumber;
  private int capacity;
  private ArrayList<Reservation> reservations = new ArrayList<>();

  public static class Reservation implements Comparable<Reservation> {
    public LocalDate date;
    public LocalTime startTime;
    public int hours;
    
    @Override
    public int compareTo(Reservation o) {
      int result = this.date.compareTo(o.date);
      if (result != 0)
        return result;
      else
        return this.startTime.compareTo(o.startTime);
    }
  }

  public Room(int roomNumber, int capacity) {
    this.roomNumber = roomNumber;
    this.capacity = capacity;
  }

  /**
   * Add a reservation to the list.
   */
  public void addReservation(Reservation res) {
    reservations.add(res);
    Collections.sort(reservations);
  }

  public LocalTime reserve(LocalDate date, int hours) {
    LocalTime startTime = null; // Pessimist - we won't find a time

    // Exercise: complete this method!
    
    return startTime;
  }
}
