package week07_Inheritance.product;

public class Electronics extends Physical {
	private String guarantee;

	public Electronics(int ID, String name) {
		super(ID, name);
	}
	
	// --- Getters and Setters ---

	public String getGuarantee() {
		return guarantee;
	}

	public void setGuarantee(String guarantee) {
		this.guarantee = guarantee;
	}
}
