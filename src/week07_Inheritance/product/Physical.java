package week07_Inheritance.product;

public abstract class Physical extends Product {
	private int shippingWeight;
	
	public Physical(int ID, String name) {
		super(ID, name);
	}	
	
	// --- Getters and Setters ---

	public int getShippingWeight() {
		return shippingWeight;
	}

	public void setShippingWeight(int shippingWeight) {
		this.shippingWeight = shippingWeight;
	}
}
