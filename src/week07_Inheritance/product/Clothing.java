package week07_Inheritance.product;

public class Clothing extends Physical {
	private String size;

	public Clothing(int ID, String name) {
		super(ID, name);
	}
	
	// --- Getters and Setters ---

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}
}
