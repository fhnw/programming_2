package week07_Inheritance.product;

public abstract class Product {
	private final int ID; // Set by the constructor
	private String name;
	private String description;
	private int price;
	
	public Product(int ID, String name) {
		this.ID = ID;
		this.name = name;
	}
	
	public boolean equals(Object o) {
		if (o == null || o.getClass() != this.getClass()) return false;
		Product p = (Product) o;
		return this.ID == p.ID;
	}
	
	@Override
	public String toString() {
		return ID + " " + name;
	}
	
	// --- Getters and Setters ---
	
	public int getID() {
		return ID;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}
	
}
