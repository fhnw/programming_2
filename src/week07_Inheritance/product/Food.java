package week07_Inheritance.product;

import java.time.LocalDate;

public class Food extends Physical {
	private LocalDate expiryDate;

	public Food(int ID, String name) {
		super(ID, name);
	}
	
	// --- Getters and Setters ---

	public LocalDate getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(LocalDate expiryDate) {
		this.expiryDate = expiryDate;
	}
}
