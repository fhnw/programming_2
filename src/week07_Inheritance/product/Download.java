package week07_Inheritance.product;

import java.net.URL;

public class Download extends Product {
	private URL url;
	
	public Download(int ID, String name, URL url) {
		super(ID, name);
		this.url = url;
	}
	
	// --- Getters and Setters ---
	
	public URL getUrl() {
		return url;
	}

	public void setUrl(URL url) {
		this.url = url;
	}
	
}
