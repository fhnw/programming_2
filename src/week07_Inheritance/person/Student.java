package week07_Inheritance.person;

public class Student extends Person {
	
	public enum Program { WI, BIT };
	
	private int studentNumber;
	private Program program;
	private int Semester;
	
	public Student(String name, int studentNumber) {
		super(name); // Call the constructor of class Person
		this.studentNumber = studentNumber;
	}
	
	// --- Getters and Setters ---

	public int getStudentNumber() {
		return studentNumber;
	}

	public void setStudentNumber(int studentNumber) {
		this.studentNumber = studentNumber;
	}

	public Program getProgram() {
		return program;
	}

	public void setProgram(Program program) {
		this.program = program;
	}

	public int getSemester() {
		return Semester;
	}

	public void setSemester(int semester) {
		Semester = semester;
	}
}
