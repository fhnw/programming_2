package week07_Inheritance.person;

import java.math.BigDecimal;

public class Teacher extends Person {
	public enum Institute { WI, FI };
	
	private int employeeID;
	private Institute institute; // Department where the teacher works
	private BigDecimal salary;
	
	public Teacher(String name, int employeeID) {
		super(name); // Call constructor of Person class
		this.salary = BigDecimal.ZERO; // Very hungry teacher
	}
	
	// --- Getters and Setters ---

	public int getEmployeeID() {
		return employeeID;
	}

	public void setEmployeeID(int employeeID) {
		this.employeeID = employeeID;
	}

	public Institute getInstitute() {
		return institute;
	}

	public void setInstitute(Institute institute) {
		this.institute = institute;
	}

	public BigDecimal getSalary() {
		return salary;
	}

	public void setSalary(BigDecimal salary) {
		this.salary = salary;
	}
}
