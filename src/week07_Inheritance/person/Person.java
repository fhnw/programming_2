package week07_Inheritance.person;

public class Person {
	private static int highestID = 0;
	private final int ID;
	private String name;
	private int address; // ID of an object of class Address (not shown)
	
	public Person(String name) {
		this.ID = highestID++;
		this.name = name;
		this.address = -1; // Invalid value, until set
	}
	
	public boolean equals(Object o) {
		if (o == null || o.getClass() != this.getClass()) return false;
		Person p = (Person) o;
		return this.ID == p.ID;
	}
	
	// --- Getters and Setters ---

	public int getID() {
		return ID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAddress() {
		return address;
	}

	public void setAddress(int address) {
		this.address = address;
	}
}
