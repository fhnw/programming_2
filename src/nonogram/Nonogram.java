package nonogram;

import javafx.application.Application;
import javafx.stage.Stage;
import nonogram.controller.Controller;
import nonogram.model.Model;
import nonogram.view.View;

public class Nonogram extends Application {

	public static void main(String[] args) {
		launch();
	}

	@Override
	public void start(Stage stage) throws Exception {
		Model m = new Model();
		View v = new View(stage, m);
		Controller c = new Controller(m, v);
		v.start();
	}
}
