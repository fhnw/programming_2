package nonogram.controller;

import javafx.animation.ParallelTransition;
import javafx.beans.Observable;
import javafx.event.ActionEvent;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import nonogram.model.Model;
import nonogram.view.Animations;
import nonogram.view.View;

public class Controller {
	private Model model;
	private View view;
	
	public Controller(Model model, View view) {
		this.model = model;
		this.view = view;
		
		new MenuController(model, view.getMenus(), this);
		
		model.gameWonProperty().addListener(this::gameWon);
	}
	
	public void startNewGame(int puzzleKey) {
		model.init(puzzleKey);
		initView();
	}
	
	public void startNewGame() {
		model.init();
		initView();
	}

	private void initView() {
		view.getPlayingGrid().initPlayingGrid();
		view.getStage().sizeToScene();
		view.getStatusBar().setPuzzleKey(model.getPuzzleKey());
		view.getStatusBar().restartTime();
	}
	
	private void gameWon(Observable o, boolean oldValue, boolean newValue) {
		if (newValue) {
			ParallelTransition wonAnimation = Animations.getShrinkAnimation(view.getPlayingGrid());
			wonAnimation.setOnFinished(this::restartGame);
			wonAnimation.play();
		}
	}
	
	private void restartGame(ActionEvent e) {
		startNewGame();
		ParallelTransition regrowAnimation = Animations.getGrowAnimation(view.getPlayingGrid());
		regrowAnimation.play();
	}
}
