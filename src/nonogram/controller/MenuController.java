package nonogram.controller;

import java.util.Optional;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextInputDialog;
import javafx.scene.input.MouseEvent;
import nonogram.model.Model;
import nonogram.view.Menus;
import nonogram.view.PuzzleKeyDialog;

public class MenuController {
	private Model model;
	private Menus menus;
	private Controller controller;

	public MenuController(Model model, Menus menus, Controller controller) {
		this.model = model;
		this.menus = menus;
		this.controller = controller;

		Menu menuGridSize = menus.getMenuGridSize();

		for (MenuItem mi : menuGridSize.getItems()) {
			mi.setOnAction(this::changeGridSize);
		}

		menus.getNewGameRandom().setOnAction(this::newGame);
		menus.getNewGamePuzzleKey().setOnAction(this::puzzleKey);
	}

	private void changeGridSize(ActionEvent e) {
		CheckMenuItem item = (CheckMenuItem) e.getSource();

		// Deselect previous
		Menu menuGridSize = menus.getMenuGridSize();
		for (MenuItem mi : menuGridSize.getItems()) {
			CheckMenuItem cmi = (CheckMenuItem) mi;
			cmi.setSelected(false);
		}

		// Select this menu-item
		item.setSelected(true);

		// Update game
		int size = Integer.parseInt(item.getText().split("x")[0]);
		if (size != model.getGridSize()) {
			model.setGridSize(size);
			controller.startNewGame();
		}
	}

	private void newGame(ActionEvent e) {
		controller.startNewGame();
	}

	private void puzzleKey(ActionEvent e) {
		PuzzleKeyDialog dialog = new PuzzleKeyDialog();
		final Optional<String> result = dialog.showAndWait();
		if (result.isPresent()) {
			int puzzleKey = Integer.parseInt(result.get());
			if (puzzleKey >= 100 && puzzleKey <= 99999999)
				// Some JavaFX operations from the dialog are apparently still pending.
				// It seems that runLater is therefore necessary; otherwise the main window
				// fails to resize correctly.
				Platform.runLater(() -> controller.startNewGame(puzzleKey));
		}
	}
}
