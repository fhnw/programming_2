package nonogram.model;

import java.util.ArrayList;
import java.util.Random;

import javafx.beans.property.SimpleBooleanProperty;

public class Model {
	public final int MIN_SIZE = 10;
	public final int MAX_SIZE = 30;
	
	private final SimpleBooleanProperty gameWonProperty = new SimpleBooleanProperty(false);

	public enum CellOption {
		EMPTY, BLOCK, RED
	};

	private int gridSize = 25; // Starting grid size

	private CellOption[][] gameGrid;
	private CellOption[][] solutionGrid;
	
	private final Random rand = new Random();
	private int puzzleKey;

	public Model() {
		init();
	}

	public void init() {
		gameWonProperty.set(false);
		initSolutionGrid();
		initGameGrid();
	}
	
	public void init(int puzzleKey) {
		gameWonProperty.set(false);
		solutionGrid = Generator.generateNonogram(gridSize, puzzleKey);
		initGameGrid();
	}

	private void initSolutionGrid() {
		boolean solvable = false;
		
		while (!solvable) {
			// For ease of use, limit keys to 8 digits
			do {
				puzzleKey = rand.nextInt();
			} while (puzzleKey < 10 | puzzleKey > 99999999);
			solutionGrid = Generator.generateNonogram(gridSize, puzzleKey);
			solvable = Solver.solve(solutionGrid);
		}
	}

	private void initGameGrid() {
		gameGrid = new CellOption[gridSize][gridSize];

		for (int i = 0; i < gameGrid.length; i++) {
			CellOption[] row = gameGrid[i];
			for (int j = 0; j < row.length; j++) {
				row[j] = CellOption.EMPTY;
			}
		}
	}

	public void setGridSize(int gridSize) {
		if (gridSize >= MIN_SIZE && gridSize <= MAX_SIZE) {
			this.gridSize = gridSize;
			initGameGrid();
		}
	}

	public int getGridSize() {
		return gridSize;
	}

	/**
	 * Ideally, we should clone the grid to prevent accidental changes, but we are
	 * not that paranoid
	 */
	public CellOption[][] getGameGrid() {
		return gameGrid;
	}

	/**
	 * Return the hint for a particular row of the nonogram, as a sequence of
	 * integers
	 * 
	 * @param row
	 *            The row for which we return the hint
	 * @return a sequence of integers representing the block-sequences in the row
	 */
	public Integer[] getRowHint(int row) {
		ArrayList<Integer> sequences = new ArrayList<>();

		boolean inSequence = (solutionGrid[row][0] == CellOption.BLOCK);
		int cursor = 1;
		int count = inSequence ? 1 : 0;
		while (cursor < gridSize) {
			if (inSequence && solutionGrid[row][cursor] == CellOption.BLOCK) {
				count++;
			} else if (inSequence && solutionGrid[row][cursor] != CellOption.BLOCK) {
				sequences.add(count);
				count = 0;
				inSequence = false;
			} else if (!inSequence && solutionGrid[row][cursor] == CellOption.BLOCK) {
				count = 1;
				inSequence = true;
			}
			cursor++;
		}
		if (inSequence) sequences.add(count);

		return sequences.toArray(new Integer[0]);
	}

	/**
	 * Return the hint for a particular column of the nonogram, as a sequence of
	 * integers
	 * 
	 * @param column
	 *            The column for which we return the hint
	 * @return a sequence of integers representing the block-sequences in the column
	 */
	public Integer[] getColumnHint(int column) {
		ArrayList<Integer> sequences = new ArrayList<>();

		boolean inSequence = (solutionGrid[0][column] == CellOption.BLOCK);
		int cursor = 1;
		int count = inSequence ? 1 : 0;
		while (cursor < gridSize) {
			if (inSequence && solutionGrid[cursor][column] == CellOption.BLOCK) {
				count++;
			} else if (inSequence && solutionGrid[cursor][column] != CellOption.BLOCK) {
				sequences.add(count);
				count = 0;
				inSequence = false;
			} else if (!inSequence && solutionGrid[cursor][column] == CellOption.BLOCK) {
				count = 1;
				inSequence = true;
			}
			cursor++;
		}
		if (inSequence) sequences.add(count);

		return sequences.toArray(new Integer[0]);
	}

	/**
	 * Check to see if the game has been won: all blocks match, all others
	 * considered empty
	 */
	private void updateGameWon() {
		boolean isWon = true;
		for (int row = 0; row < gridSize && isWon; row++) {
			for (int col = 0; col < gridSize && isWon; col++) {
				isWon = ((solutionGrid[row][col] == CellOption.BLOCK && gameGrid[row][col] == CellOption.BLOCK)
						|| (solutionGrid[row][col] != CellOption.BLOCK && gameGrid[row][col] != CellOption.BLOCK));
			}
		}
		gameWonProperty.set(isWon);
	}
	
	/**
	 * Check the contents of a single row
	 */
	public boolean verifyRow(int row) {
		boolean isCorrect = true;
		for (int col = 0; col < gridSize && isCorrect; col++) {
			isCorrect = ((solutionGrid[row][col] == CellOption.BLOCK && gameGrid[row][col] == CellOption.BLOCK)
					|| (solutionGrid[row][col] != CellOption.BLOCK && gameGrid[row][col] != CellOption.BLOCK));		}
		return isCorrect;
	}
	
	/**
	 * Check the contents of a single column
	 */
	public boolean verifyCol(int col) {
		boolean isCorrect = true;
		for (int row = 0; row < gridSize && isCorrect; row++) {
			isCorrect = ((solutionGrid[row][col] == CellOption.BLOCK && gameGrid[row][col] == CellOption.BLOCK)
					|| (solutionGrid[row][col] != CellOption.BLOCK && gameGrid[row][col] != CellOption.BLOCK));		}
		return isCorrect;
	}

	/**
	 * Process a click from the user. If the cell is already set to the given
	 * option, then clear the cell.
	 */
	public void click(int col, int row, CellOption co) {
		if (gameGrid[row][col] == co) co = CellOption.EMPTY;
		gameGrid[row][col] = co;
		updateGameWon();
	}
	
	/**
	 * Process a drag from the user. This sets the cell to the given value,
	 * regardless of its current value.
	 */
	public void drag(int col, int row, CellOption co) {
		gameGrid[row][col] = co;
		updateGameWon();
	}
	
	/**
	 * Get a cell value
	 */
	public CellOption getCell(int col, int row) {
		return gameGrid[row][col];
	}
	
	/**
	 * Set a cell to the given value, with no further processing. This is not from a user interaction, but from automation in the view.
	 */
	public void setCell(int col, int row, CellOption co) {
		gameGrid[row][col] = co;
	}

	public CellOption getCellOption(int col, int row) {
		return gameGrid[row][col];
	}
	
	public SimpleBooleanProperty gameWonProperty() { return gameWonProperty; }
	
	public long getPuzzleKey() {
		return puzzleKey;
	}
}
