package nonogram.view;

import java.time.LocalTime;
import java.time.temporal.ChronoUnit;

import javafx.application.Platform;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import nonogram.model.Model;

public class StatusBar extends HBox {
	private Model model;

	private Label lblPuzzleKey = new Label();
	private Label lblTime = new Label();
	private LocalTime startTime = LocalTime.now();

	public StatusBar(Model model) {
		super();
		this.model = model;
		
		Region spacer = new Region();
		HBox.setHgrow(spacer, Priority.ALWAYS);
		this.getChildren().addAll(lblPuzzleKey, spacer, lblTime);	
		setPuzzleKey(model.getPuzzleKey());
		
		// Start time-updating thread
		startTimeUpdater();	
	}
	
	public void restartTime() {
		startTime = LocalTime.now();
	}
	
	public void setPuzzleKey(long puzzleKey) {
		lblPuzzleKey.setText("Puzzle " + puzzleKey);
	}
	
	public void startTimeUpdater() {
		Thread t = new Thread(() -> {
			while(true) {
				long seconds = ChronoUnit.SECONDS.between(startTime,  LocalTime.now());
				long minutes = seconds / 60;
				seconds = seconds % 60;
				long hours = minutes / 60;
				minutes = minutes % 60;
				
				String displayTime = String.format("%02d", hours) + ":" + String.format("%02d", minutes) + ":" + String.format("%02d", seconds);
				Platform.runLater(() -> { lblTime.setText(displayTime); });
				try {
					Thread.sleep(200);
				} catch (InterruptedException e) {
				}
			}
		});
		t.setDaemon(true);
		t.start();
	}
}
