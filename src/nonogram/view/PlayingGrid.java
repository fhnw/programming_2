package nonogram.view;

import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import nonogram.model.Model;
import nonogram.model.Model.CellOption;

public class PlayingGrid extends GridPane {
	Model model;
	private View view;

	private final String highlightColStyle = "highlight-col";
	private final String highlightRowStyle = "highlight-row";
	private final String correctStyle = "correct";

	// Maintain easy-access arrays of the row/column header cells,
	// with 0-based indices
	private Label[] rowLabels;
	private Label[] colLabels;
	
	// Maintain easy-access array of the cells in the game-grid
	Cell[][] gameCells;

	public PlayingGrid(Model model, View view) {
		super();
		this.model = model;
		this.view = view;

		initPlayingGrid();

		this.getStyleClass().add("grid");
	}

	/**
	 * This method initializes and re-initializes the playing grid. It is used both
	 * when the program starts, and any time a new grid is needed for a new game.
	 * 
	 * Note that the playing grid has an extra row and column, as compared to the
	 * grid in the model.
	 */
	public void initPlayingGrid() {
		// Remove any previous stuff
		this.getChildren().clear();
		this.getColumnConstraints().clear();
		this.getRowConstraints().clear();

		CellOption[][] grid = model.getGameGrid();

		// Prep arrays to hold header cells and game cells
		rowLabels = new Label[model.getGridSize()];
		colLabels = new Label[model.getGridSize()];
		gameCells = new Cell[model.getGridSize()][model.getGridSize()];

		// Place top-left cell
		TopLeftCell tfc = new TopLeftCell();
		this.add(tfc, 0, 0);
		GridPane.setMargin(tfc, new Insets(0, 1, 1, 0));

		// Place column labels
		for (int col = 1; col <= model.getGridSize(); col++) {
			ColumnLabel cl = new ColumnLabel(model.getColumnHint(col - 1));
			colLabels[col - 1] = cl;
			this.add(cl, col, 0);
			if (col % 5 == 0) {
				GridPane.setMargin(cl, new Insets(0, 1, 1, 0));
			} else {
				GridPane.setMargin(cl, new Insets(0, 0, 1, 0));
			}
		}

		// Add rows from model
		for (int row = 1; row <= model.getGridSize(); row++) {
			RowLabel rl = new RowLabel(model.getRowHint(row - 1));
			rowLabels[row - 1] = rl;
			if (row % 5 == 0) {
				GridPane.setMargin(rl, new Insets(0, 1, 1, 0));
			} else {
				GridPane.setMargin(rl, new Insets(0, 1, 0, 0));
			}
			this.add(rl, 0, row);
			CellOption[] modelRow = grid[row - 1];
			for (int col = 1; col <= model.getGridSize(); col++) {
				Cell cell = new Cell(modelRow[col - 1], this, col, row);
				this.add(cell, col, row);
				if (col % 5 == 0 && row % 5 != 0) GridPane.setMargin(cell, new Insets(0, 1, 0, 0));
				else if  (col % 5 != 0 && row % 5 == 0) GridPane.setMargin(cell, new Insets(0, 0, 1, 0));
				else if  (col % 5 == 0 && row % 5 == 0) GridPane.setMargin(cell, new Insets(0, 1, 1, 0));
				// else normal cell, no extra margin
				gameCells[row-1][col-1] = cell;
			}
		}

		// Create column and row constraints
		this.getColumnConstraints().add(new ColumnConstraints());
		this.getRowConstraints().add(new RowConstraints());
		for (int i = 1; i <= model.getGridSize(); i++) {
			if (i % 5 == 0) {
				this.getColumnConstraints().add(new ColumnConstraints(31));
				this.getRowConstraints().add(new RowConstraints(31));
			} else {
				this.getColumnConstraints().add(new ColumnConstraints(30));
				this.getRowConstraints().add(new RowConstraints(30));
			}
		}
	}

	public void setHighlight(boolean highlight, int col, int row) {
		for (Node node : this.getChildren()) {
			boolean matchRow = this.getRowIndex(node) == row;
			boolean matchCol = this.getColumnIndex(node) == col;
			if (matchRow || matchCol) {
				if (highlight) {
					if (matchRow && !matchCol)
						node.getStyleClass().add(highlightRowStyle);
					else if (matchCol && !matchRow) {
						node.getStyleClass().add(highlightColStyle);
					}
				} else {
					node.getStyleClass().remove(highlightRowStyle);
					node.getStyleClass().remove(highlightColStyle);
				}
			}
		}
	}
	
	/**
	 * On a drag, we set the value of the cell to a particular option.
	 * We then re-check the marking
	 * of correct rows/cols.
	 */
	public void drag(Cell cell, int col, int row, CellOption co) {
		model.drag(col - 1, row - 1, co);
		cell.displayCellOption(co);
		
		checkCorrect();
	}

	/**
	 * On a click, we flip the value of the cell. We then re-check the marking
	 * of correct rows/cols.
	 */
	public void click(Cell cell, int col, int row, CellOption co) {
		model.click(col - 1, row - 1, co);
		co = model.getCellOption(col - 1, row - 1);
		cell.displayCellOption(co);
		
		checkCorrect();
	}
	
	private void checkCorrect() {
		// Remove "correct" style for all row/col header cells
		for (Label rl : rowLabels)
			rl.getStyleClass().remove(correctStyle);
		for (Label cl : colLabels)
			cl.getStyleClass().remove(correctStyle);
		
		// If the option to mark finished rows/cols is selected, check them
		// all. Also mark any empty cells in finished rows/cols as RED
		if (view.getMenus().isMarkFinished()) {
			for (int i = 0; i < model.getGridSize(); i++) {
				if (model.verifyRow(i)) {
					rowLabels[i].getStyleClass().add(correctStyle);
					markEmptyRowCells(i);
				} else {
					rowLabels[i].getStyleClass().remove(correctStyle);
				}
				if (model.verifyCol(i)) {
					colLabels[i].getStyleClass().add(correctStyle);
					markEmptyColCells(i);
				} else {
					colLabels[i].getStyleClass().remove(correctStyle);
				}
			}
		}
	}

	/**
	 * In a finished row, we can mark all empty cells as RED
	 */
	private void markEmptyRowCells(int row) {
		for (int col = 0; col < model.getGridSize(); col++) {
			if (model.getCell(col, row) == CellOption.EMPTY) {
				model.setCell(col, row, CellOption.RED);
				gameCells[row][col].displayCellOption(CellOption.RED);
			}
		}
	}

	/**
	 * In a finished row, we can mark all empty cells as RED
	 */
	private void markEmptyColCells(int col) {
		for (int row = 0; row < model.getGridSize(); row++) {
			if (model.getCell(col, row) == CellOption.EMPTY) {
				model.setCell(col, row, CellOption.RED);
				gameCells[row][col].displayCellOption(CellOption.RED);
			}
		}
	}
}
