package nonogram.view;

import javafx.scene.control.Label;
import javafx.scene.layout.Region;

public class ColumnLabel extends Label {
	public ColumnLabel(Integer[] sequences) {
		super();
		this.getStyleClass().add("grid-cell");
		this.getStyleClass().add("col-hint");
		
		this.setMinHeight(Region.USE_PREF_SIZE);
		
		StringBuffer hint = new StringBuffer();
		for (Integer sequence : sequences) {
			if (hint.length() != 0) hint.append('\n');
			hint.append(sequence);
		}
		
		this.setText(hint.toString());
	}
}
