package nonogram.view;

import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import nonogram.model.Model;
import nonogram.model.Model.CellOption;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

/**
 * This class violates the MVC model, because it acts as its own controller.
 * This is simpler than creating a separate controller class with a separate
 * object for each cell.
 */
public class Cell extends Pane {
	// When dragging across multiple cells, we need to record what
	// the current drag is doing
	private static CellOption currentDrag = null;

	PlayingGrid playingGrid;
	private int col;
	private int row;

	// In the cell where the drag starts, we have to be careful not to set the cell
	// twice. We reset this when the mouse leaves this cell (MouseExited/DragExited)
	private boolean isDragDoneHere = false;

	public Cell(PlayingGrid playingGrid, int col, int row) {
		super();
		this.playingGrid = playingGrid;
		this.row = row;
		this.col = col;

		this.setOnMousePressed(this::setCell);
		this.setOnMouseReleased(e -> isDragDoneHere = false);
		this.setOnMouseEntered(e -> this.highlight(true));
		this.setOnMouseExited(e -> {
			this.highlight(false);
			isDragDoneHere = false;
		});

		// Enable MouseDrag events, when the user starts to drag
		// At this point, MousePressed has already fired, so the
		// value of the current cell is also the drag action
		this.setOnDragDetected(e -> {
			this.startFullDrag();
			currentDrag = playingGrid.model.getCellOption(col - 1, row - 1);
		});

		this.setOnMouseDragEntered(this::dragCell);
		this.setOnMouseDragExited(e -> isDragDoneHere = false);

		this.getStyleClass().add("grid-cell");
	}

	public Cell(CellOption co, PlayingGrid playingGrid, int col, int row) {
		this(playingGrid, col, row);
		displayCellOption(co);
	}

	public void displayCellOption(CellOption co) {
		this.getChildren().clear();
		this.getStyleClass().clear();
		if (co == CellOption.BLOCK) {
			this.getStyleClass().add("dark-grid-cell");
		} else {
			this.getStyleClass().add("grid-cell");
			if (co == CellOption.RED) {
				this.getChildren().add(new Circle(15, 15, 5, Color.RED));
			}
		}
	}

	private void setCell(MouseEvent e) {
		if (!isDragDoneHere) {
			CellOption co = CellOption.BLOCK; // assume left-click
			if (e.getButton() == MouseButton.SECONDARY) co = CellOption.RED;
			playingGrid.click(this, col, row, co);
			isDragDoneHere = true;
		}
	}

	private void dragCell(MouseEvent e) {
		if (!isDragDoneHere) {
			playingGrid.drag(this, col, row, currentDrag);
			isDragDoneHere = true;
		}
	}

	/**
	 * Highlight or de-highlight the row and column of this cell
	 * 
	 * @param highlight
	 *            true if the row and column of this cell should be highlighted
	 */
	private void highlight(boolean highlight) {
		playingGrid.setHighlight(highlight, col, row);
	}
}
