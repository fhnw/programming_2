package nonogram.view;

import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import nonogram.model.Model;

public class Menus extends MenuBar {
	private Model model;
	private Menu menuGridSize = new Menu("Size");
	private Menu menuNewGame = new Menu("New game");
	private MenuItem miNewGameRandom = new MenuItem("Random");
	private MenuItem miNewGamePuzzleKey = new MenuItem("Enter puzzle key");
	private Menu menuOptions = new Menu("Options");
	private CheckMenuItem miMarkFinished = new CheckMenuItem("Mark finished rows/cols");

	public Menus(Model model) {
		super();
		
		this.model = model;
		
		for (int i = model.MIN_SIZE; i <= model.MAX_SIZE; i += 5) {
			CheckMenuItem cmi = new CheckMenuItem(i + "x" + i);
			if (model.getGridSize() == i) cmi.setSelected(true);
			menuGridSize.getItems().add(cmi);
		}
		this.getMenus().add(menuGridSize);

		// New game menu
		menuNewGame.getItems().addAll(miNewGameRandom, miNewGamePuzzleKey);
		this.getMenus().add(menuNewGame);
		
		// Options menu
		this.getMenus().add(menuOptions);
		menuOptions.getItems().add(miMarkFinished);
		miMarkFinished.setSelected(true); // Default to marking finished rows/cols
	}
	
	public Menu getMenuGridSize() {
		return menuGridSize;
	}
	
	public MenuItem getNewGameRandom() {
		return miNewGameRandom;
	}
	
	public MenuItem getNewGamePuzzleKey() {
		return miNewGamePuzzleKey;
	}
	
	public boolean isMarkFinished() {
		return miMarkFinished.isSelected();
	}
}
