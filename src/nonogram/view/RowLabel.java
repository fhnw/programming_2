package nonogram.view;

import javafx.scene.control.Label;
import javafx.scene.layout.Region;

public class RowLabel extends Label {
	public RowLabel(Integer[] sequences) {
		super();
		this.getStyleClass().add("grid-cell");
		this.getStyleClass().add("row-hint");
		
		this.setMinWidth(Region.USE_PREF_SIZE);
		
		StringBuffer hint = new StringBuffer();
		for (Integer sequence : sequences) {
			if (hint.length() != 0) hint.append(' ');
			hint.append(sequence);
		}
		
		this.setText(hint.toString());
	}
}
