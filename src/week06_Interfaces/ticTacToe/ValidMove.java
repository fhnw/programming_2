package week06_Interfaces.ticTacToe;

/**
 * Defines the moves that are possible
 */
public enum ValidMove {
	X, O
}
