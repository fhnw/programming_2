package week06_Interfaces.ticTacToe;

/**
 * Represents a TicTacToe game as an array of ValidMove objects. Where no move
 * has been made, the array entry is null. This class also defines the following
 * methods:
 * 
 * - String gameOver() returns a String with the game result, or null if the
 * game is not over
 * 
 * - ValidMove[][] getBoard() returns a *copy* of the current board
 * 
 * - boolean makeMove(GameMove) makes the given move and returns true (if
 * valid); otherwise does nothing and returns false
 */
public class GameLogic {
	private ValidMove[][] board = new ValidMove[3][3];

	/** Default constructor, shown for completeness */
	public GameLogic() {
	}

	/**
	 * Return a read-only copy of the game board
	 */
	public ValidMove[][] getBoard() {
		ValidMove[][] copy = new ValidMove[3][3];
		for (int col = 0; col < 3; col++) {
			for (int row = 0; row < 3; row++) {
				copy[col][row] = board[col][row];
			}
		}
		return copy;
	}

	/**
	 * Check columns, rows and diagonals for three identical symbols. If no winner found, check for draw.
	 */
	public String getGameResult() {
		ValidMove winner = null;
		String result = null;
		
		// Rows and columns
		for (int i = 0; i < 3; i++) {
			ValidMove columnWinner = board[i][0];
			ValidMove rowWinner = board[0][i];
			for (int j = 1; j < 3; j++) {
				if (columnWinner != board[i][j]) columnWinner = null;
				if (rowWinner != board[j][i]) rowWinner = null;
			}
			if (columnWinner != null) winner = columnWinner;
			if (rowWinner != null) winner = rowWinner;
		}
		
		// Diagonals
		if (winner == null) {
			ValidMove diagWinner1 = board[0][0];
			ValidMove diagWinner2 = board[2][0];
			for (int i = 1; i < 3; i++) {
				if (diagWinner1 != board[i][i]) diagWinner1 = null;
				if (diagWinner2 != board[2-i][i]) diagWinner2 = null;
			}
			if (diagWinner1 != null) winner = diagWinner1;
			if (diagWinner2 != null) winner = diagWinner2;
		}
		
		// If winner, then announce it. Otherwise, check for draw
		if (winner != null) {
			result = winner.toString() + " wins!";
		} else {
			// Check for draw
			boolean emptySpotFound = false;
			for (int col = 0; col < 3; col++) {
				for (int row = 0; row < 3; row++) {
					emptySpotFound |= (board[col][row] == null);
				}
			}
			if (!emptySpotFound) result = "Draw!";
		}

		return result;
	}

	/**
	 * Place the given move onto the board
	 * 
	 * @return true if move is valid, false if move cannot be made
	 */
	public boolean makeMove(GameMove move) {
		boolean valid = (board[move.getCol()][move.getRow()] == null);
		if (valid) board[move.getCol()][move.getRow()] = move.getMove();
		return valid;
	}
}
