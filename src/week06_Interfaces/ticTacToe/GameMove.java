package week06_Interfaces.ticTacToe;

public class GameMove {
	private int row;
	private int col;
	private ValidMove move;
	
	public GameMove(int col, int row, ValidMove move) {
		this.col = col;
		this.row = row;
		this.move = move;
	}

	public int getRow() {
		return row;
	}

	public int getCol() {
		return col;
	}

	public ValidMove getMove() {
		return move;
	}
}
