package week06_Interfaces.chatBot;

public class Insulting implements Personality {

	@Override
	public String getGreeting() {
		return pickRandom(greetings);
	}

	@Override
	public String answerQuestion() {
		return pickRandom(answers);
	}

	@Override
	public String getFarewell() {
		return pickRandom(farewells);
	}

	@Override
	public String getOther() {
		return pickRandom(others);
	}

	private String pickRandom(String[] texts) {
		int pick = (int) (Math.random() * texts.length);
		return texts[pick];
	}
	
	private String[] greetings = {
			"Bugger off, I was sleeping",
			"Whaddya want",
			"Great, you're here, can you leave now?",
			"How lovely to chat with you. Not.",
			"Surprise, surprise, I didn't think you could type."
	};

	private String[] answers = {
			"What, do you think I'm here to serve you?",
			"Seriously?",
			"Go ask Wikipedia",
			"How am I supposed to answer that?",
			"Do I look like I care?"
	};
	
	private String[] farewells = {
			"Great, good riddance",
			"Don't come back soon, okay?",
			"Get lost"
	};
	
	private String[] others = {
			"They do say users are getting dumber, and you're the proof",
			"You're not the dumbest person on the planet, but you'd better hope he doesn't die",
			"Everybody makes mistakes; some people *are* mistakes.",
			"Wow. I'm a computer and you've bored me to sleep. Zzzz"
	};
}
