package week06_Interfaces.chatBot;

public class Friendly implements Personality {

	@Override
	public String getGreeting() {
		return pickRandom(greetings);
	}

	@Override
	public String answerQuestion() {
		return pickRandom(answers);
	}

	@Override
	public String getFarewell() {
		return pickRandom(farewells);
	}

	@Override
	public String getOther() {
		return pickRandom(others);
	}

	private String pickRandom(String[] texts) {
		int pick = (int) (Math.random() * texts.length);
		return texts[pick];
	}
	
	private String[] greetings = {
			"Hi, pleased to chat with you!",
			"How are you doing today?",
			"Great to see you!"
	};

	private String[] answers = {
			"Sorry, I didn't get that...",
			"I don't know - I am a bot of very little brain",
			"Um...no idea"
	};
	
	private String[] farewells = {
			"Bye for now",
			"Have a nice day!",
			"Goodbye, come back soon"
	};
	
	private String[] others = {
			"Hey, when the chicken crossed the road, he got to the other side!",
			"Anyway, what's so special about 42?",
			"My chips itch...sorry, what did you say?"
	};
}
