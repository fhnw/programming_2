package week06_Interfaces.chatBot;

public interface Personality {
	public abstract String getGreeting();
	public abstract String answerQuestion();
	public abstract String getFarewell();
	public abstract String getOther();
}
