package week06_Interfaces;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class ListExample {
	private static final Random random = new Random();

	public static void main(String[] args) {
		List<Integer> list1 = getLinkedList();
		List<Integer> list2 = getArrayList();
		
		// Add all elements from list2 to list1
		for (Integer i : list2) list1.add(i);
		
		// Sort list1
		Collections.sort(list1);
		
		// Print list1
		for (Integer i : list1) System.out.print(i + " ");
	}
	
	private static LinkedList<Integer> getLinkedList() {
		LinkedList<Integer> numbers = new LinkedList<>();
		for (int i = 0; i < 5; i++) numbers.add(random.nextInt(100));
		return numbers;
	}

	private static ArrayList<Integer> getArrayList() {
		ArrayList<Integer> numbers = new ArrayList<>();
		for (int i = 0; i < 5; i++) numbers.add(random.nextInt(100));
		return numbers;
	}
}
