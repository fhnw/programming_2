package week06_Interfaces.api_example;

/**
 * This interface defines a standard API for arithmetic operations.
 */
public interface Arithmetic {
	public int add(int x, int y);
	public int subtract(int x, int y);
	public int multiply(int x, int y);
	public int divide(int x, int y);
}
