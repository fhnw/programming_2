package week06_Interfaces.api_example;

/**
 * Acme is a company that provides Java libraries.
 * This library provides basic arithmetic
 * operations for primitive integers
 */
public class Acme_services implements Arithmetic {

	@Override
	public int add(int x, int y) {
		return x + y;
	}

	@Override
	public int subtract(int x, int y) {
		return x - y;
	}

	@Override
	public int multiply(int x, int y) {
		return x * y;
	}

	@Override
	public int divide(int x, int y) {
		return x / y;
	}
}
