package week06_Interfaces.solutions;

public class Box implements Comparable<Box> {
	private int width;
	private int height;
	private int depth;
	
	public Box(int width, int height, int depth) {
		this.width = width;
		this.height = height;
		this.depth = depth;
	}
	
	@Override
	public boolean equals(Object o) {
		boolean isEqual = false;
		if (o != null && o instanceof Box) {
			Box b = (Box) o;
			isEqual = (this.width == b.width && this.height == b.height && this.depth == b.depth);
		}
		return isEqual;
	}
	
	@Override
	public int compareTo(Box b) {
		return this.getVolume() - b.getVolume();
	}
		
	// Getters and setters
	
	public int getVolume() {
		return width * height * depth;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getDepth() {
		return depth;
	}

	public void setDepth(int depth) {
		this.depth = depth;
	}
}
