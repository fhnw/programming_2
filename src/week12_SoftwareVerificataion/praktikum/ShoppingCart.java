package week12_SoftwareVerificataion.praktikum;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Achtung: Diese Klasse beinhaltet Fehler!!!
 */
public class ShoppingCart {
	private int sessionID;
	private final ArrayList<Item> items = new ArrayList<>();
	
	public ShoppingCart() {
		
	}
	
	public class Item {
		int productID;
		int quantity;
		int price; // in Rappen
		
		public Item(int productID, int quantity, int price) {
			this.productID = productID;
			this.quantity = quantity;
			this.price = price;
		}
	}
	
	public boolean addItem(int productID, int quantity, int price) {
		boolean ok = productID > 0 && quantity > 0 && price > 100;
		if (ok) items.add(new Item(productID, quantity, price));
		return ok;
	}
	
	public boolean removeItem(int productID) {
		boolean found = false;
		for (Iterator<Item> i = items.iterator(); !found && i.hasNext(); ) {
			Item item = i.next();
			if (item.productID == productID) {
				i.remove();
				found = true;
			}
		}
		return found;
	}
	
	public boolean changeQuantity(int productID, int newQuantity) {
		boolean found = false;
		if (newQuantity > 0 && newQuantity < 1000) {
			for (Iterator<Item> i = items.iterator(); !found && i.hasNext(); ) {
				Item item = i.next();
				if (item.productID == productID) {
					item.quantity = newQuantity;
					found = true;
				}
			}
		}
		return found;
	}
	
	public int total() {
		int total = 0;
		for (Item item : items) {
			total += item.price * item.quantity;
		}
		return total;
	}
	
	//--- Getters and Setters ---
	
	public ShoppingCart(int sessionID) {
		this.sessionID = sessionID;
	}

	public int getSessionID() {
		return sessionID;
	}

	public void setSessionID(int sessionID) {
		this.sessionID = sessionID;
	}

	public ArrayList<Item> getItems() {
		return items;
	}
}
