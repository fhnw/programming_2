package week12_SoftwareVerificataion.praktikum;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ShoppingCartTest {
	ShoppingCart cart;

	@BeforeEach
	void setUp() throws Exception {
		cart = new ShoppingCart();
	}

	@Test
	void testProductID() {
		assertTrue(cart.addItem(1,  1,  100));
		assertTrue(cart.addItem(356,  1,  100));
		assertFalse(cart.addItem(0, 1, 100));
		assertFalse(cart.addItem(-50, 1, 100));
	}
	
	@Test
	void testQuantity() {
		assertTrue(cart.addItem(1, 1, 100));
		assertTrue(cart.addItem(1, 5, 100));
		assertTrue(cart.addItem(1, 1000, 100));
		assertFalse(cart.addItem(1, 0, 100));
		assertFalse(cart.addItem(1, -5, 100));
		assertFalse(cart.addItem(1, 1001, 100));
		assertFalse(cart.addItem(1, 10000, 100));
	}

}
