package week12_SoftwareVerificataion;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;

class DateAndTimeMethodsTest {

	@Test
	void testBasic() {
		LocalDate onDate = LocalDate.of(2015, 6, 15);
        assertEquals(5, DateAndTimeMethods.getAge(LocalDate.of(2010, 3, 3), onDate));
        assertEquals(4, DateAndTimeMethods.getAge(LocalDate.of(2010, 8, 8), onDate));
        assertEquals(5, DateAndTimeMethods.getAge(LocalDate.of(2010, 6, 3), onDate));
        assertEquals(4, DateAndTimeMethods.getAge(LocalDate.of(2010, 6, 23), onDate));
	}
	
	@Test
	void testNegative() {
		LocalDate onDate = LocalDate.of(2015, 6, 15);
        Exception e = assertThrows(Exception.class, () -> DateAndTimeMethods.getAge2(LocalDate.of(2020, 3, 3), onDate));
		assertEquals("Not born!", e.getMessage()); // Optional: Check information in the exception
	}
}
