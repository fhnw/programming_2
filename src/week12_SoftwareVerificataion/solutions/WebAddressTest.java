package week12_SoftwareVerificataion.solutions;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class WebAddressTest {
	private WebAddressValidator wav = new WebAddressValidator();

	private String[] validNumericAddresses = {
			"0.0.0.0", "255.255.255.255", "127.0.0.1",
			"000.000.000.000", "234.234.234.234"
	};

	private String[] invalidNumericAddresses = {
			"0.0.0", "255.255.255", "256.0.0.1",
			"127.0.0.256", "8.8", "8", "8.8.8.8.8"
	};
	
	private String[] validSymbolicAddresses = {
			"google.com", "www.google.com",
			"some.admin.somewhere.edu", "ch.ch"
	};

	private String[] invalidSymbolicAddresses = {
			".google.com", "google.com.",
			"google,com", "com", "com.", ".com"
	};

	@Test
	void testVNA() {
		for (String s : validNumericAddresses) {
			assertTrue(wav.isValidWebAddress(s));
		}
	}

	@Test
	void testINA() {
		for (String s : invalidNumericAddresses) {
			assertFalse(wav.isValidWebAddress(s));
		}
	}

	@Test
	void testVSA() {
		for (String s : validSymbolicAddresses) {
			assertTrue(wav.isValidWebAddress(s));
		}
	}

	@Test
	void testISA() {
		for (String s : invalidSymbolicAddresses) {
			assertFalse(wav.isValidWebAddress(s));
		}
	}
}
