package week12_SoftwareVerificataion.solutions;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

/**
 * This is a simple set of tests - no need for data preparation
 */
class PortNumberTest {
	private WebAddressValidator wav = new WebAddressValidator();
	
	// Non-numeric tests
	@Test
	void testNonNumeric() {
		assertFalse(wav.isValidPortNumber(null));
		assertFalse(wav.isValidPortNumber(""));
		assertFalse(wav.isValidPortNumber("x"));
		assertFalse(wav.isValidPortNumber("123x"));
		assertFalse(wav.isValidPortNumber("x123"));
	}	
	
	// Numeric tests
	@Test
	void testNumeric() {
		assertFalse(wav.isValidPortNumber("-100"));
		assertFalse(wav.isValidPortNumber("0"));
		assertTrue(wav.isValidPortNumber("1"));
		assertTrue(wav.isValidPortNumber("5000"));
		assertTrue(wav.isValidPortNumber("65535"));
		assertFalse(wav.isValidPortNumber("65536"));
		assertFalse(wav.isValidPortNumber("1000000"));
	}

}
