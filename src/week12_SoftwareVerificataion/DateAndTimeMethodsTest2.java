package week12_SoftwareVerificataion;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;
import java.util.ArrayList;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class DateAndTimeMethodsTest2 {
	// Simple class to store one test case
	private static class TestCase {
		LocalDate birthDate;
		LocalDate onDate;
		int age;
	}

	// ArrayList to store lots of test cases
	private static ArrayList<TestCase> testCases;
	
	// Raw test data (could also be read from a file)
	private static int[][] testDataRaw = {
		    // Basic tests
		    { 2010, 3, 3, 2015, 6, 15, 5 },
		    { 2010, 8, 8, 2015, 6, 15, 4 },
		    { 2010, 6, 3, 2015, 6, 15, 5 },
		    { 2010, 6, 23, 2015, 6, 15, 4 },
		        
		    // Boundary tests
		    { 2010, 5, 23, 2015, 6, 15, 5 },
		    { 2010, 7, 1, 2015, 6, 15, 4 },
		    { 2010, 6, 15, 2015, 6, 15, 5 },
		    { 2010, 6, 16, 2015, 6, 15, 4 },
		    { 2015, 6, 15, 2015, 6, 15, 0 },
		       
		    // An age of -1 indicates errors
		    // These should generate exceptions
		    { 2015, 6, 16, 2015, 6, 15, -1 },
		    { 2020, 7, 23, 2015, 6, 15, -1 }
		  };	
	
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		testCases = new ArrayList<>();
		for (int[] t : testDataRaw) {
			TestCase testCase = new TestCase();
			testCase.birthDate = LocalDate.of(t[0], t[1], t[2]);
			testCase.onDate = LocalDate.of(t[3], t[4], t[5]);
			testCase.age = t[6];
			testCases.add(testCase);
		} 
	}

	@Test
	void test() {
		for (TestCase t : testCases) {
			try {
				assertEquals(t.age, DateAndTimeMethods.getAge2(t.birthDate, t.onDate));
				if (t.age == -1) fail(); // Should have had an exception
			} catch (Exception e) {
				assertEquals(t.age, -1); // If age is -1, then an exception is correct
			}
		}
	}
}
