package week02.MVC.tutorial.v1;

import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Mines_View {
	private Mines_Model model;
	protected Stage stage;
	
	protected MenuBar menus = new MenuBar();
	protected Menu menuFile = new Menu("Game");
	protected MenuItem miAbout = new MenuItem("About");
	
	protected VBox rightPane = new VBox();
	protected Label lblGameOver = new Label("Playing");
	
	protected GridPane gameGrid = new GridPane();
	protected Button[][] buttons; // initialized in the "new game" method
	
	public Mines_View(Stage stage, Mines_Model model) {
		this.stage = stage;
		this.model = model;

		BorderPane root = new BorderPane();

		// Create and add menus
		menus.getMenus().add(menuFile);
		menuFile.getItems().add(miAbout);
		root.setTop(menus);
		
		// Add VBox on the right
		rightPane.getChildren().add(lblGameOver);
		root.setRight(rightPane);
		
		// Add game grid
		root.setCenter(gameGrid);
		
		Scene scene = new Scene(root);
		stage.setTitle("Mines Game");
		scene.getStylesheets().add(
				getClass().getResource("mines.css").toExternalForm());
		stage.setScene(scene);;
	}

	public void start() {
		stage.show();
	}
	
	/**
	 * Initialize a new game. Called from the controller.
	 */
	protected void newGame(int width, int height) {
		gameGrid.getChildren().clear();
		buttons = new Button[height][width];
		for (int row = 0; row < height; row++) {
			for (int col = 0; col < width; col++) {
				Button btn = new Button();
				buttons[row][col] = btn;
				gameGrid.add(btn, col, row);
			}
		}
	}
	
	protected void showBoom() {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("Boom!!!");
		alert.setHeaderText("You hit a mine!");
		alert.setContentText("Game over, you have been blown up!");
		alert.showAndWait();
	}
}
