package week02.MVC.tutorial.v2;

public class Mines_Model {
	private class Cell {
		public boolean isMine = false;
		public boolean isRevealed = false;
	}
	
	private Cell[][] gameBoard;
	
	public static class Boom extends Exception {
		public Boom() {
			super("Mine found!");
		}
	}
	
	public void newGame(int width, int height) {
		gameBoard = new Cell[height][width];
		for (int row = 0; row < height; row++) {
			for (int col = 0; col < width; col++) {
				gameBoard[row][col] = new Cell();
			}
		}
		
		// Initialize game with one mine for every 6 fields
		for (int i = 0; i < width*height/6; i++) {
			int row = (int) (Math.random() * height);
			int col = (int) (Math.random() * width);
			gameBoard[row][col].isMine = true;
		}		
	}
	
	public int revealCell(int row, int col) throws Boom {
		gameBoard[row][col].isRevealed = true; // In v1 this line was in the wrong place !!

		if (gameBoard[row][col].isMine) throw new Boom();
		
		return countMines(row, col);
	}
	
	/**
	 * This method counts the number of neighboring cells that contain mines
	 */
	private int countMines(int row, int col) {
		int minRow = Math.max(0, row - 1);
		int maxRow = Math.min(gameBoard.length - 1, row + 1);
		int minCol = Math.max(0, col - 1);
		int maxCol = Math.min(gameBoard[0].length - 1, col + 1);

		int count = 0;
		for (int r = minRow; r <= maxRow; r++) {
			for (int c = minCol; c <= maxCol; c++) {
				if (gameBoard[r][c].isMine) count++;
			}
		}
		return count;
	}
	
	/**
	 * A game is over if (a) any bomb has been revealed, or (b)
	 * all non-bombs have been revealed.
	 */
	protected boolean isGameOver() {
		boolean bombRevealed = false;
		boolean nonBombHidden = false;
		
		for (int row = 0; row < gameBoard.length; row++) {
			for (int col = 0; col < gameBoard[0].length; col++) {
				Cell cell = gameBoard[row][col];
				bombRevealed |= cell.isMine && cell.isRevealed;
				nonBombHidden |= !cell.isMine && !cell.isRevealed;
			}
		}
		
		return (bombRevealed || !nonBombHidden);		
	}
}
