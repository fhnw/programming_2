This MVC example is developed in a video tutorial that you will find on Moodle.
Specifically, this is a simple version of the "Mines" or "Minesweeper" game.

Video "Part 0": Planning and design

Video "Part 1": Basic program structure, static View (package "v0")

Video "Part 2": Creates a new game, can left-click on cells  (package "v1")

No video: Can right-click on (package "v2")

Remaining functionality not implemented in the tutorial...