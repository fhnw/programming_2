package week11_networking.webServer;

import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Logger;

public class Model {
	private Integer port;
	private String webRoot; // Location of files

	public Model(Integer port, String webRoot) {
		this.port = port;
		this.webRoot = webRoot;
	}

	public void start() {
		try (ServerSocket listener = new ServerSocket(port, 10, null)) {
			while (true) {				
				Socket socket = listener.accept(); // Wait for request				
				Client client = new Client(socket, webRoot); // Serve the client
				client.start();
			}
		} catch (Exception e) {
			System.err.println(e);
		}
	}
}
