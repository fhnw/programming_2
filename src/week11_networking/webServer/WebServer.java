package week11_networking.webServer;

/**
 * When started on a server, we cannot have a GUI. This class provides
 * a way to run the web-server from the command line.
 * 
 * By default, the program will expect web-root in the www subdirectory
 * of whatever directory is is started in; it will then ask the user for
 * a port number.
 * 
 * Alternatively, both port number and web-root (absolute path) can be
 * provided as command-line parameters.
 */
public class WebServer {

	public static void main(String[] args) {
		// Port number is 8080, unless specified on the command line
		int portNumber = 8080;
		if (args.length > 0) portNumber = Integer.parseInt(args[0]);
		
		// Web root is "www", unless specified on the command line
		String webRoot = "www";
		if (args.length > 1) webRoot = args[1];

		// Create the model
		Model model = new Model(portNumber, webRoot);
		
		// Start serving requests
		model.start();
	}
}
