package week11_networking.https_browser;

public class BrowserController {

	final private BrowserModel model;
	final private BrowserView view;

	protected BrowserController(BrowserModel model, BrowserView view) {
		this.model = model;
		this.view = view;

		// register ourselves to listen for button clicks
		view.btnGo.setOnAction(e -> {
			String ipAddress = view.txtIP.getText();
			String webPage = model.browse(ipAddress);
			view.txtWebPage.setText(webPage);
		});
	}
}
