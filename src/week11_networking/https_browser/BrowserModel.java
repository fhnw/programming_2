package week11_networking.https_browser;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

import javax.net.ssl.HttpsURLConnection;

public class BrowserModel {
	public String browse(String ipAddress) {
		String lineIn;
		StringBuffer pageContent = new StringBuffer();

		// Network errors are always possible
		try {
			URL url = new URL("https://" + ipAddress); // HTTPS port defaults to 443
			HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();

			// For a simple GET, the following lines are not actually necessary.
			// GET is the default. This shows how we can set protocol options
			connection.setRequestMethod("GET");
			connection.setRequestProperty("Accept", "text/html, text/plain");
			
			// Say we are firefox
			// connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0");

			// Start reading
			BufferedReader inReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			while ((lineIn = inReader.readLine()) != null) {
				pageContent.append(lineIn + "\n");
			}
		}
		// If an error occurred, show the error message in txtInhalt
		catch (Exception err) {
			pageContent.append("ERROR: " + err.toString());
		}

		return pageContent.toString();
	}
}
