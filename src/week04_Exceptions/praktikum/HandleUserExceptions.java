package week04_Exceptions.praktikum;

import java.io.IOException;
import java.util.NoSuchElementException;
import java.util.Scanner;

/** Goal: Read a number between 1 and 100, inclusive.
 * Stop on a zero. See what kinds of exceptions can occur.
 * 
 * Use Try-with-resources to close our Scanner
 */
public class HandleUserExceptions {
	public static class OutOfRangeException extends IOException {
		public OutOfRangeException(String msg) {
			super(msg);
		}
	}

	public static void main(String[] args) {
		try (Scanner in = new Scanner(System.in)) {
			int value = -1;
			while (value != 0) {
				System.out.println("Enter your birth year, I will tell you your age this year (0 to stop)");
				
				try {
				String s = in.next(); // Can generate NoSuchElementException, if input is closed
				value = Integer.parseInt(s); // Can generate NumberFormatException
				if (value != 0 && (value < 1900 || value > 2100)) throw new OutOfRangeException("value out of range");
				} catch (OutOfRangeException e) {
					System.out.println("Value must be between 1900 and 2100, or 0 to stop");
				} catch (NumberFormatException e) {
					System.out.println("Please enter an integer");
				} catch (NoSuchElementException e) {
					System.out.println("You closed the input; next time please enter 0");
					value = 0;
				}
			}
			System.out.println("Stopping...");
		} catch (Exception e) {
			System.out.println("Exception not caught! " + e.toString());
		}
	}

}
