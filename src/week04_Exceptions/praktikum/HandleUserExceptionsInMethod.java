package week04_Exceptions.praktikum;

import java.util.NoSuchElementException;
import java.util.Scanner;

import week04_Exceptions.praktikum.HandleUserExceptions.OutOfRangeException;

public class HandleUserExceptionsInMethod {

	public static void main(String[] args) {
		try (Scanner in = new Scanner(System.in)) {
			int value = -1;
			while (value != 0) {
				System.out.println("Enter your birth year, I will tell you your age this year (0 to stop)");
				
				try {
				String s = in.next(); // Can generate NoSuchElementException, if input is closed
				value = parseUserInput(s); // Can generate OutOfRangeException or NumberFormatException
				if (value != 0 && (value < 1900 || value > 2100)) throw new OutOfRangeException("value out of range");
				} catch (OutOfRangeException e) {
					System.out.println("Value must be between 1900 and 2100, or 0 to stop");
				} catch (NumberFormatException e) {
					System.out.println("Please enter an integer");
				} catch (NoSuchElementException e) {
					System.out.println("You closed the input; next time please enter 0");
					value = 0;
				}
			}
			System.out.println("Stopping...");
		} catch (Exception e) {
			System.out.println("Exception not caught! " + e.toString());
		}
	}

	private static int parseUserInput(String s) throws OutOfRangeException, NumberFormatException {
		int value = Integer.parseInt(s);
		if (value < 0 || value > 100) throw new OutOfRangeException("value out of range");
		return value;
	}
}
