package week14.oopExample;

import java.util.ArrayList;

public class Room {
	private String roomNumber;
	private ArrayList<Equipment> equipment;
	
	public Room(String roomNumber) {
		this.roomNumber = roomNumber;
		this.equipment = new ArrayList<>();
	}
	
	public String getRoomNumber() {
		return roomNumber;
	}
	
	public ArrayList<Equipment> getEquipment() {
		return equipment;
	}
}
