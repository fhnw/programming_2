package week14.oopExample;

public class Classroom extends Room implements Chargeable {

	public Classroom(String roomNumber) {
		super(roomNumber);
	}

	@Override
	public int getCostPerYear() {
		int total = 1000;
		for (Equipment e : this.getEquipment()) {
			total += e.getCost();
		}
		return total;
	}

}
