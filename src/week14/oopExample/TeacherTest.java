package week14.oopExample;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class TeacherTest {

	@Test
	void test() {
		Teacher t1 = new Teacher("fred@fhnw.ch", "123456");
		t1.setName("fred");
		Teacher t2 = new Teacher("sue@fhnw.ch", "123457");
		t2.setName("sue");

		assertNotEquals(t1.getID(), t2.getID());
		assertEquals(t1.getCostPerYear(), 50000);
		t1.setAge(54);
		assertEquals(t1.getCostPerYear(), 104000);
		t2.setAge(37);
		assertEquals(t2.getCostPerYear(), 87000);		
	}

}
