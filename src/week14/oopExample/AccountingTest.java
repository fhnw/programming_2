package week14.oopExample;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class AccountingTest {
	@Test
	void teacherTest() {
		Accounting acc = new Accounting();
		assertEquals(acc.roomCost(), 0);
		assertEquals(acc.teacherCost(), 0);
		
		Teacher t = new Teacher("a@fhnw.ch", "123");
		t.setAge(40);
		assertTrue(acc.add(t));
		assertEquals(acc.teacherCost(), 90000);
		Teacher t2 = new Teacher("b@fhnw.ch", "124");
		t2.setAge(50);
		assertTrue(acc.add(t2));
		assertEquals(acc.teacherCost(), 190000);
		assertFalse(acc.add(t));
		assertEquals(acc.teacherCost(), 190000);
	}

	@Test
	void roomTest() {
		Accounting acc = new Accounting();
		assertEquals(acc.roomCost(), 0);
		assertEquals(acc.teacherCost(), 0);
		
		Classroom r = new Classroom("6.1D53");
		assertTrue(acc.add(r));
		assertEquals(acc.roomCost(), 1000);
		r.getEquipment().add(Equipment.Laptop);
		assertEquals(acc.roomCost(), 2000);
		r.getEquipment().add(Equipment.Projector);
		assertEquals(acc.roomCost(), 2500);
	}
}
