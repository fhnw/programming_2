package week14.oopExample;

public class Teacher extends Person implements Chargeable {
	private String employeeNr;
	private int age = 0;

	// Constructor goes here
	public Teacher(String email, String employeeNr) {
		super();
		this.setEmail(email);
		this.employeeNr = employeeNr;
	}
	
	// Cost per year: 50000 + 1000 * age
	public int getCostPerYear() {
		return 50000 + 1000 * age;
	}

	// Assume getters and setters already exist
	public String getEmployeeNr() {
		return employeeNr;
	}

	public void setEmployeeNr(String employeeNr) {
		this.employeeNr = employeeNr;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}
}
