package week14.oopExample;

import java.util.ArrayList;

public class Accounting {
	private ArrayList<Chargeable> items;
	
	// Constructor must initialize ArrayList
	public Accounting() {
		items = new ArrayList<>();
	}
	
	// Add item only if not present; return true if successful
	public boolean add(Chargeable item) {
		if (items.contains(item)) {
			return false;
		} else {
			items.add(item);
			return true;
		}
	}
	
	// Total cost of all teachers in items-list
	public int teacherCost() {
		int total = 0;
		for (Chargeable item : items) {
			if (item.getClass() == Teacher.class) {
				total += item.getCostPerYear();
			}
		}
		return total;
	}
	
	// Total cost of all classrooms in items-list
	public int roomCost() {
		int total = 0;
		for (Chargeable item : items) {
			if (item instanceof Room) {
				total += item.getCostPerYear();
			}
		}
		return total;
	}
}
