package week14.oopExample;

public abstract class Person {
	private static int nextID = 0;
	
	private final int ID;
	private String name;
	private String email;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getID() {
		return ID;
	}

	public Person() {
		this.ID = nextID++;
	}
}
