package week14.oopExample;

public interface Chargeable {
	public int getCostPerYear();
}
