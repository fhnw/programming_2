package week14.oopExample;

public enum Equipment {
	Laptop(1000), Projector(500);
	
	private int cost;
	
	private Equipment(int cost) {
		this.cost = cost;
	}
	
	public int getCost() {
		return cost;
	}
}
